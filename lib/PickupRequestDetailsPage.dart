import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/Common.dart';

class PickupRequestDetailsPage extends StatefulWidget {
  const PickupRequestDetailsPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PickupRequestDetailsWidgetState();
}

class _PickupRequestDetailsWidgetState extends State<PickupRequestDetailsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Pickup Request Details'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Container(),
    );
  }
}
