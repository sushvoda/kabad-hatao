import 'package:flutter/material.dart';
import 'package:kabadhatao/Drawer.dart';
import 'package:kabadhatao/Images.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'StringFile.dart';

class MoreOptionsPage extends StatefulWidget {
  const MoreOptionsPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MoreOptionsPageWidgetState();
}

class _MoreOptionsPageWidgetState extends State<MoreOptionsPage> {
  String userName = "";
  String userMobile = "";
  String profilePic = "";

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    getValues();
    super.initState();
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userName = sharedPreferences.getString("name") ?? kabadHatao;
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    profilePic = sharedPreferences.getString("profilePic") ?? "";

    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer(
        context: context,
        userName: userName,
        userMobile: userMobile,
        profilePic: profilePic,
        isPartner: false,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).viewPadding.top),
        child: Column(
          children: [
            IconButton(
              icon: svgDrawer,
              onPressed: () => _scaffoldKey.currentState?.openDrawer(),
            ),
          ],
        ),
      ),
    );
  }
}
