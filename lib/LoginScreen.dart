import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/ColorFile.dart';
import 'package:kabadhatao/Images.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';

import './ColorFile.dart';
import 'Common.dart';
import 'LoginScreenOTP.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Logo(),
                const Text(
                  "Your Home & Office Clean Service Expert",
                  style: TextStyle(
                      fontSize: 17, fontFamily: 'GOTHAM', color: textColor),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 4),
                  child: const Text(
                    "EASY RETURN \u00b7 FREE PICKUP \u00b7 BEST RATE",
                    style: TextStyle(
                        fontSize: 12,
                        fontFamily: 'GOTHAM',
                        color: textColorLite),
                  ),
                ),
                Container(
                  child: TextField(
                    onChanged: (text) {
                      if (text.length == 10) {
                        signup(context, text.toString());
                      }
                    },
                    style: const TextStyle(fontSize: 15),
                    keyboardType: TextInputType.number,
                    cursorColor: primaryColor,
                    decoration: const InputDecoration(
                      prefixText: '+91',
                      border: OutlineInputBorder(),
                      labelText: 'Mobile Number',
                    ),
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                  ),
                  margin: const EdgeInsets.only(top: 30),
                  width: 350,
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              alignment: Alignment.bottomRight,
              height: MediaQuery.of(context).size.height,
              child: MaterialButton(
                onPressed: () => {dashboardPage(context)},
                child: const Text(
                  "Skip",
                  style: TextStyle(
                      fontSize: 17, fontFamily: 'GOTHAM', color: primaryColor),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> signup(context, mobile) async {
    FocusScope.of(context).unfocus();

    try {
      var dio = Dio();
      if (kDebugMode) {
        print(signupUrl);
      }
      await LoaderUtils().showAlertDialog(context);
      final response = await dio.post(signupUrl, data: {'phoneNo': mobile});
      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          otpPage(context, mobile, response.data['data']["otp"],
              response.data['data']["id"]);
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }

  void otpPage(context, text, otp, id) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (BuildContext context) => LoginScreenOTP(text, otp, id)),
    );
  }
}

class Logo extends StatelessWidget {
  const Logo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(child: Image(image: logoImage), height: 250, width: 250);
  }
}
