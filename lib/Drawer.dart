import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kabadhatao/AllServicesPage.dart';
import 'package:kabadhatao/Common.dart';
import 'package:kabadhatao/HelpAndSupportPage.dart';
import 'package:kabadhatao/PersonalInformationPage.dart';
import 'package:kabadhatao/utils/URLConstants.dart';

import 'AllServicesNewPage.dart';
import 'ColorFile.dart';
import 'DownloadInvoicePage.dart';
import 'Images.dart';

Drawer drawer({context, userName, userMobile, profilePic, isPartner = false}) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        UserAccountsDrawerHeader(
          decoration: BoxDecoration(
              color: transparent,
              image: DecorationImage(image: drawerBg, fit: BoxFit.cover)),
          accountEmail: Container(),
          accountName: Row(
            children: <Widget>[
              profilePic == null || profilePic.isEmpty
                  ? SizedBox(
                      width: 50,
                      height: 50,
                      child: CircleAvatar(
                        backgroundColor: grayLiteBG,
                        child: SvgPicture.asset(
                          'images/user_icon.svg',
                          color: Colors.white,
                          width: 30.0,
                          height: 30.0,
                        ),
                      ),
                    )
                  : ClipOval(
                      child: Image.network(
                        url + profilePic,
                        fit: BoxFit.cover,
                        width: 50,
                        height: 50,
                      ),
                    ),
              const SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  makeText(
                    label: userName,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                  makeText(
                    label: userMobile,
                    color: Colors.white,
                    fontSize: 12.0,
                  ),
                ],
              ),
            ],
          ),
        ),
        /*UserAccountsDrawerHeader(
          accountEmail: Text(
            userMobile,
            style: const TextStyle(color: Colors.white),
          ),
          accountName: Text(
            userName,
            style: const TextStyle(fontSize: 24.0, color: Colors.white),
          ),
          currentAccountPicture: Image(image: logoImage, height: 50, width: 50),
          decoration: const BoxDecoration(color: primaryDarkColor),
        ),*/
        Card(
          child: ListTile(
            contentPadding: const EdgeInsets.all(10.0),
            dense: true,
            leading: SvgPicture.asset(
              'images/price_list.svg',
              color: primaryColor,
              width: 25.0,
              height: 25.0,
            ),
            title: const Text('Become a partner'),
            subtitle: makeText(
                label: 'You can switch to partner account & earn money',
                fontSize: 10.0),
            trailing: Switch(
              value: isPartner,
              onChanged: (bool value) {
                isPartner = value;
              },
              activeColor: primaryColor,
            ),
          ),
        ),
        ListTile(
          leading: SvgPicture.asset(
            'images/home_drawer.svg',
            width: 20.0,
            color: drawerIconColor,
            height: 20.0,
          ),
          title: const Text(
            'Order',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {},
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: SvgPicture.asset(
            'images/home_drawer.svg',
            width: 20.0,
            color: drawerIconColor,
            height: 20.0,
          ),
          title: const Text(
            'Home',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {},
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: const Icon(Icons.info_outlined),
          title: const Text(
            'Personal Information',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {
            openPage(context, const PersonalInformationPage());
          },
          trailing: const Icon(Icons.chevron_right),
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: const Icon(Icons.location_on_outlined),
          title: const Text(
            'Pickup Track',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {},
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: const Icon(Icons.article_outlined),
          title: const Text(
            'Download Invoice',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {
            openPage(context, const DownloadInvoicePage());
          },
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: SvgPicture.asset(
            'images/help_and_support.svg',
            color: drawerIconColor,
            width: 20.0,
            height: 20.0,
          ),
          title: const Text(
            'Help and Support',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {
            openPage(context, const HelpAndSupportPage());
          },
          trailing: const Icon(Icons.chevron_right),
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: SvgPicture.asset(
            'images/our_services.svg',
            color: drawerIconColor,
            width: 20.0,
            height: 20.0,
          ),
          title: const Text(
            'Our Services',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {
            openPage(context, const AllServicesNewPage());
          },
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: SvgPicture.asset(
            'images/contact_us.svg',
            color: drawerIconColor,
            width: 20.0,
            height: 20.0,
          ),
          title: const Text(
            'Contact Us',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {},
        ),
        const Divider(height: 10, thickness: 1),
        ListTile(
          leading: SvgPicture.asset(
            'images/feedback.svg',
            color: drawerIconColor,
            width: 20.0,
            height: 20.0,
          ),
          title: const Text(
            'Feedback',
            style: TextStyle(fontSize: 16.0),
          ),
          onTap: () {},
        ),
      ],
    ),
  );
}

void openPage(BuildContext context, onPage) {
  Navigator.of(context).pop();
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (BuildContext context) => onPage));
}

void openPageNew(BuildContext context, onPage) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (BuildContext context) => onPage));
}
