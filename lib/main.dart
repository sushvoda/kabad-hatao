import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/ColorFile.dart';
import 'package:kabadhatao/Common.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './ColorFile.dart';
import 'Images.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kabad Hatao Pvt Ltd.',
      theme: ThemeData(
          primaryColor: primaryColor,
          primaryColorDark: primaryDarkColor,
          colorScheme:
              ColorScheme.fromSwatch().copyWith(secondary: accentColor)),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var _duration = const Duration(seconds: 5);
    return Timer(_duration, navigationPage);
  }

  navigationPage() async {
    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      bool isLoggedIn = sharedPreferences.getBool("is_logged_in") ?? false;

      if (isLoggedIn) {
        dashboardPage(context);
      } else {
        welcomePage(context);
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      welcomePage(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: primaryDarkColor,
        statusBarBrightness: Brightness.dark,
      ),
    );
    return Scaffold(
      body: Stack(
        children: [
          Image(
              image: splashBg,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: splashLogo,
                  width: 200,
                  height: 200,
                )
              ],
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: const [
                Text(
                  "@2020-2021 KabadHatao Pvt. Ltd.",
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: 'GOTHAM',
                    color: primaryColor,
                  ),
                ),
                SizedBox(height: 40),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
