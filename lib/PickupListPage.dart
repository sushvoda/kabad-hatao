import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/ColorFile.dart';
import 'package:kabadhatao/Common.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/date_time_format.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model/models.dart';

class PickupListPage extends StatefulWidget {
  const PickupListPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PickupListPageWidgetState();
}

class _PickupListPageWidgetState extends State<PickupListPage> {
  int userId = 0;

  List<OrderModel> orderList = [];

  late OrderModel selectedOrder;

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getInt("id") ?? 0;

    Future.delayed(Duration.zero, () {
      getOrderList();
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    getValues();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Pickup List'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Builder(
        builder: (context) => Stack(
          children: [
            SingleChildScrollView(
              physics: const ScrollPhysics(),
              child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: orderList.length,
                itemBuilder: (context, position) {
                  return Card(
                    margin: const EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                    elevation: 5.0,
                    child: SizedBox(
                      width: double.infinity,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(5.0)),
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  const Text(
                                    "Pickup Request",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  const Spacer(),
                                  Text(
                                    "ORDER # ${orderList[position].id}",
                                    style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                convertDateFormat(orderList[position].date!, "yyyy-MMM-dd HH:mm:ss zzz", "yyyy-MM-dd"),
                                    style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  const Spacer(),
                                  const Text(
                                    "View order details",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: primaryColor),
                                  ),
                                ],
                              ),
                              const Divider(color: Colors.grey),
                              Row(
                                children: [
                                  Image.network(
                                    url + orderList[position].imageUrl!,
                                    width: 100,
                                    errorBuilder:
                                        (BuildContext context, Object exception, StackTrace? stackTrace) {
                                      // Appropriate logging or analytics, e.g.
                                      // myAnalytics.recordError(
                                      //   'An error occurred loading "https://example.does.not.exist/image.jpg"',
                                      //   exception,
                                      //   stackTrace,
                                      // );
                                      return const Text('No Image Found');
                                    },
                                  ),
                                ],
                              ),
                              const Divider(color: Colors.grey),
                              Row(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Pickup Request date : ${convertDateFormat(orderList[position].date!, "yyyy-MMM-dd HH:mm:ss zzz", "EEE dd MMM yyyy")}",
                                        style: const TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.grey),
                                      ),
                                      const SizedBox(height: 5),
                                      const DecoratedBox(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5.0)),
                                            color: primaryColor),
                                        child: Padding(
                                          padding: EdgeInsets.all(10.0),
                                          child: Text(
                                            "Track Pickup Request",
                                            style: TextStyle(
                                                fontSize: 10,
                                                fontWeight: FontWeight.normal,
                                                color: Colors.white),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const Spacer(),
                                  const VerticalDivider(color: Colors.grey),
                                  const Spacer(),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      const Text(
                                        "Expected Pickup : Within 24 Hrs",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.grey),
                                      ),
                                      const SizedBox(height: 5),
                                      GestureDetector(
                                        onTap: () {
                                          selectedOrder = orderList[position];
                                          cancelOrder(context);
                                        },
                                        child: DecoratedBox(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(5.0)),
                                            border:
                                                Border.all(color: primaryColor),
                                          ),
                                          child: const Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Text(
                                              "Cancel Pickup Request",
                                              style: TextStyle(
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.black),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            Visibility(
                child: const Center(
                  child: Text(
                    "No Pickup Found",
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
                visible: orderList.isEmpty)
          ],
        ),
      ),
    );
  }

  void getOrderList() async {
    await LoaderUtils().showAlertDialog(context);
    try {
      var dio = Dio();
      var url = orderListUrl + "$userId";
      if (kDebugMode) {
        print(url);
      }
      var response = await dio.get(url);
      if (kDebugMode) {
        print(response.data);
        print(response.data["data"]["order_list"]);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          if (response.data["data"]["order_list"] != null) {
            var rest = response.data["data"]["order_list"] as List;

            List<OrderModel> list = rest
                .map<OrderModel>((json) => OrderModel.fromJson(json))
                .toList();
            list = list.where((map) => map.cancelled == false).toList();
            orderList.addAll(list);
            setState(() {});

            if (kDebugMode) {
              print(orderList.isNotEmpty ? orderList[0] : "");
            }
          } else {
            message("No pickup found.", context);
          }
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }

  void cancelOrder(BuildContext context) async {
    try {
      var dio = Dio();
      var url = cancelOrderUrl + selectedOrder.id.toString();
      if (kDebugMode) {
        print(url);
      }
      await LoaderUtils().showAlertDialog(context);
      Response response;
      response = await dio.patch(url);

      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          message("Order cancelled successfully.", context);
          Navigator.pop(context, response.data);
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }
}
