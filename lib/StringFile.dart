const String supportMail = 'info@kabadhatao.com';
const String mailSubject = 'Feedback for Kabad Hatao';
const String mailBody = 'Please white your query here';
const String supportWhatsapp = '+919891746770';
const String whatsappMessage = 'Please write your query here';
const String webUrl = "www.kabadhatao.com";
const String kabadHatao = "Kabad Hatao";
