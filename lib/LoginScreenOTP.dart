import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/ColorFile.dart';
import 'package:kabadhatao/Common.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:kabadhatao/utils/preference.dart';

import './ColorFile.dart';

class LoginScreenOTP extends StatefulWidget {
  String mobileNumber;
  String otp;
  int id;

  LoginScreenOTP(this.mobileNumber, this.otp, this.id, {Key? key})
      : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginScreenOTP> {
  late String mobileNumber;

  final interval = const Duration(seconds: 1);

  final int timerMaxSeconds = 60;
  int currentSeconds = 0;
  late Timer _timer;

  String get timerText =>
      '${((timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')}:'
      ' ${((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  startTimeout() {
    var duration = interval;
    _timer = Timer.periodic(duration, (timer) {
      setState(() {
        if (kDebugMode) {
          print(timer.tick);
        }
        currentSeconds = timer.tick;
        if (timer.tick >= timerMaxSeconds) timer.cancel();
      });
    });
  }

  TextEditingController otpController1 = TextEditingController();
  TextEditingController otpController2 = TextEditingController();
  TextEditingController otpController3 = TextEditingController();
  TextEditingController otpController4 = TextEditingController();
  TextEditingController otpController5 = TextEditingController();
  TextEditingController otpController6 = TextEditingController();

  @override
  void initState() {
    mobileNumber = widget.mobileNumber;
    startTimeout();
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    otpController1.dispose();
    otpController2.dispose();
    otpController3.dispose();
    otpController4.dispose();
    otpController5.dispose();
    otpController6.dispose();
    cancelTimer();
    super.dispose();
  }

  void cancelTimer() {
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    // fetchPost(mobileNumber,random(1000, 9999));
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: textColor),
          onPressed: () => {
            cancelTimer(),
            Navigator.of(context).pop(),
          },
        ),
        title: const Text(
          "Login / Signup",
          style: TextStyle(
            fontFamily: 'GOTHAM',
            color: textColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 30),
            child: Center(
              child: Column(
                children: [
                  const Text(
                    "Enter verification code",
                    style: TextStyle(
                      fontSize: 30,
                      fontFamily: 'GOTHAMMEDIUM',
                      color: textColor,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 4),
                    child: const Text(
                      "We have sent you 6 digit verification code on",
                      style: TextStyle(
                        fontSize: 12,
                        fontFamily: 'GOTHAM',
                        color: textColorLite,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: Text(
                      "+91 " + mobileNumber,
                      style: const TextStyle(
                        fontSize: 20,
                        fontFamily: 'GOTHAM',
                        color: textColorLite,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 35, right: 35, top: 30),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            OTPDigitTextFieldBox(
                              first: true,
                              last: false,
                              controller: otpController1,
                            ),
                            OTPDigitTextFieldBox(
                              first: false,
                              last: false,
                              controller: otpController2,
                            ),
                            OTPDigitTextFieldBox(
                              first: false,
                              last: false,
                              controller: otpController3,
                            ),
                            OTPDigitTextFieldBox(
                              first: false,
                              last: false,
                              controller: otpController4,
                            ),
                            OTPDigitTextFieldBox(
                              first: false,
                              last: false,
                              controller: otpController5,
                            ),
                            SizedBox(
                              height: 50,
                              child: AspectRatio(
                                aspectRatio: 1.0,
                                child: TextField(
                                  autofocus: true,
                                  controller: otpController6,
                                  onChanged: (value) {
                                    validateOtp();
                                  },
                                  showCursor: false,
                                  readOnly: false,
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  maxLength: 1,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.zero,
                                    counter: const Offstage(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            width: 1, color: textColorLite),
                                        borderRadius: BorderRadius.circular(5)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            width: 1, color: textColorLite),
                                        borderRadius: BorderRadius.circular(5)),
                                    hintText: "*",
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: Text(
                      timerText,
                      style: const TextStyle(
                        fontSize: 20,
                        fontFamily: 'GOTHAM',
                        color: textColorLite,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 25),
                    child: ElevatedButton(
                      onPressed: () {
                        startTimeout();
                        signup(context, mobileNumber);
                        //fetchPost(mobileNumber, "0000120300");
                      },
                      child: const Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Text(
                          "Resend Code",
                          style: TextStyle(
                            fontSize: 18,
                            fontFamily: 'GOTHAM',
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void validateOtp() {
    String value = otpController1.text +
        otpController2.text +
        otpController3.text +
        otpController4.text +
        otpController5.text +
        otpController6.text;

    if (value.isEmpty) {
      message("Please enter otp", context);
    } else if (value.length == 6) {
      if (value == widget.otp) {
        validateOtpApi(value);
      } else {
        message("Please enter valid otp", context);
      }
    } else {
      message("Please enter valid otp", context);
    }
  }

  Future<void> validateOtpApi(otp) async {
    try {
      var dio = Dio();
      if (kDebugMode) {
        print(verifyOtpUrl);
      }
      await LoaderUtils().showAlertDialog(context);
      final response = await dio.post(verifyOtpUrl,
          data: {'phoneNo': mobileNumber, 'id': widget.id, 'otp': otp});
      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          Preference().setUserDetails(response.data['data']['userProfile']);
          dashboardPage(context);
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }

  Future<void> signup(context, mobile) async {
    try {
      var dio = Dio();
      if (kDebugMode) {
        print(signupUrl);
      }
      await LoaderUtils().showAlertDialog(context);
      final response = await dio.post(signupUrl, data: {'phoneNo': mobile});
      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          message("Otp resent successfully.", context);
          widget.id = response.data['data']["id"];
          widget.otp = response.data['data']["otp"];
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }
}

class OTPDigitTextFieldBox extends StatelessWidget {
  final bool first;
  final bool last;
  final TextEditingController controller;

  OTPDigitTextFieldBox(
      {required this.first, required this.last, required this.controller});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: AspectRatio(
        aspectRatio: 1.0,
        child: TextField(
          autofocus: true,
          controller: controller,
          onChanged: (value) {
            if (value.length == 1 && last == false) {
              FocusScope.of(context).nextFocus();
            }
            if (value.isEmpty && first == false) {
              FocusScope.of(context).previousFocus();
            }
          },
          showCursor: false,
          readOnly: false,
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly,
          ],
          maxLength: 1,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.zero,
            counter: const Offstage(),
            enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(width: 1, color: textColorLite),
                borderRadius: BorderRadius.circular(5)),
            focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(width: 1, color: textColorLite),
                borderRadius: BorderRadius.circular(5)),
            hintText: "*",
          ),
        ),
      ),
    );
  }
}
