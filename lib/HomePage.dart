import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AllServicesNewPage.dart';
import 'ColorFile.dart';
import 'Common.dart';
import 'Drawer.dart';
import 'Images.dart';
import 'ProductCategoryPage.dart';
import 'StringFile.dart';
import 'model/models.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePage> {
  int _currentBannerPage = 0;
  late PageController _bannerPageController;
  late Timer _bannerTimer;

  bool isPartner = false;

  String userName = "";
  String userMobile = "";
  String profilePic = "";
  int userId = 0;

  List<ProductCategoryModel> categoryList = [];

  late ProductCategoryModel selectedCategory;

  String cityNameValue = 'Delhi';

  List<Widget> bannerPages = [
    Image.asset('images/banner1.jpg', fit: BoxFit.fitWidth),
    Image.asset('images/banner2.jpg', fit: BoxFit.fitWidth),
    Image.asset('images/banner3.jpg', fit: BoxFit.fitWidth),
    Image.asset('images/banner4.jpg', fit: BoxFit.fitWidth),
    Image.asset('images/banner5.jpg', fit: BoxFit.fitWidth),
  ];

  List<String> cityNames = [
    'Delhi',
    'Mumbai',
    'Bangalore',
    'Hyderabad',
    'Ahmedabad',
    'Chennai',
    'Kolkata',
    'Surat',
    'Pune',
    'Jaipur',
    'Lucknow',
    'Kanpur',
    'Nagpur',
    'Indore',
    'Thane',
    'Bhopal',
    'Visakhapatnam',
    'Patna',
    'Vadodara',
    'Ghaziabad',
    'Ludhiana',
    'Agra',
    'Nashik	',
    'Faridabad',
    'Meerut',
    'Rajkot',
    'Varanasi',
    'Srinagar',
    'Aurangabad',
    'Dhanbad',
    'Amritsar',
    'Allahabad',
    'Ranchi',
    'Coimbatore',
    'Jabalpur',
    'Gwalior',
    'Jodhpur',
    'Vijayawada',
    'Madurai',
    'Raipur',
    'Kota',
    'Guwahati',
    'Chandigarh',
    'Solapur',
    'Bareilly',
    'Moradabad',
    'Gurgaon',
    'Aligarh',
    'Jalandhar',
    'Tiruchirappalli',
    'Bhubaneswar',
    'Thiruvananthapuram',
    'Saharanpur',
    'Gorakhpur',
    'Amravati',
    'Noida',
    'Jamshedpur',
    'Cuttack',
    'Firozabad',
    'Kochi',
    'Dehradun',
    'Ajmer',
    'Gulbarga',
    'Jamnagar',
    'Ujjain',
    'Loni',
    'Siliguri',
    'Jhansi',
    'Jammu',
    'Belgaum',
    'Mangalore',
    'Gaya',
    'Udaipur',
  ];

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    getValues();
    super.initState();
    _bannerPageController = PageController(initialPage: _currentBannerPage);

    _bannerTimer = Timer.periodic(const Duration(seconds: 5), (Timer timer) {
      if (_currentBannerPage < 4) {
        _currentBannerPage++;
      } else {
        _currentBannerPage = 0;
      }

      _bannerPageController.animateToPage(
        _currentBannerPage,
        duration: const Duration(milliseconds: 350),
        curve: Curves.easeIn,
      );
    });
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userName = sharedPreferences.getString("name") ?? kabadHatao;
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    profilePic = sharedPreferences.getString("profilePic") ?? "";
    userId = sharedPreferences.getInt("id") ?? 0;

    Future.delayed(Duration.zero, () {
      getCategoryList();
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _bannerPageController.dispose();
    _bannerTimer.cancel();
  }

  void onBannerPageChanged(int page) {
    setState(() {
      _currentBannerPage = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer(
        context: context,
        userName: userName,
        userMobile: userMobile,
        profilePic: profilePic,
        isPartner: isPartner,
      ),
      /*appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            IconButton(
              icon: svgDrawer,
              onPressed: () => _scaffoldKey.currentState?.openDrawer(),
            ),
            SizedBox(
              width: 140,
              height: 60.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  makeText(label: 'Your City', fontSize: 10.0),
                  DropdownButtonFormField<String>(
                    hint: const Text('Select'),
                    items: cityNames.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: makeText(
                          label: value,
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                        ),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        cityNameValue = newValue!;
                      });
                    },
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        isCollapsed: true,
                        isDense: true),
                  ),
                ],
              ),
            ),
            const Spacer(flex: 1),
            logoKH(logoKHImage),
            CircleAvatar(
              backgroundColor: transparent,
              radius: 30,
              child: svgCart, //Text
            ),
          ],
        ),
      ),*/
      body: Padding(
        padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).viewPadding.top),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: PageView(
                    children: bannerPages,
                    onPageChanged: onBannerPageChanged,
                    controller: _bannerPageController,
                  ),
                ),
                Expanded(flex: 1, child: Container()),
              ],
            ),
            Row(
              children: [
                IconButton(
                  icon: svgDrawer,
                  onPressed: () => _scaffoldKey.currentState?.openDrawer(),
                ),
                SizedBox(
                  width: 140,
                  height: 60.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      makeText(label: 'Your City', fontSize: 10.0),
                      makeText(label: 'Delhi', fontSize: 12.0),
                      // DropdownButtonFormField<String>(
                      //   hint: const Text('Delhi'),
                      //   items: cityNames.map((String value) {
                      //     return DropdownMenuItem<String>(
                      //       value: value,
                      //       child: makeText(
                      //         label: value,
                      //         fontSize: 12.0,
                      //         fontWeight: FontWeight.bold,
                      //       ),
                      //     );
                      //   }).toList(),
                      //   onChanged: (String? newValue) {
                      //     setState(() {
                      //       cityNameValue = newValue!;
                      //     });
                      //   },
                      //   decoration: const InputDecoration(
                      //       border: InputBorder.none,
                      //       isCollapsed: true,
                      //       isDense: true),
                      // ),
                    ],
                  ),
                ),
                const Spacer(flex: 1),
                logoKH(logoKHImage),
                CircleAvatar(
                  backgroundColor: transparent,
                  radius: 30,
                  child: InkWell(
                    onTap: () {
                      myCart(context);
                    },
                    child: svgCart,
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(flex: 4, child: Container()),
                Expanded(
                  flex: 2,
                  child: SingleChildScrollView(
                    child: Row(
                      children: <Widget>[
                        const SizedBox(width: 10.0),
                        Image.asset('images/slider1.png'),
                        const SizedBox(width: 10.0),
                        Image.asset('images/slider2.png'),
                        const SizedBox(width: 10.0),
                        Image.asset('images/slider3.png'),
                        const SizedBox(width: 10.0),
                      ],
                    ),
                    scrollDirection: Axis.horizontal,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: /* Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          applicationColumn(
                            label: 'Computer',
                            image: 'images/computer.svg',
                          ),
                          applicationColumn(
                            label: 'Plastic',
                            image: 'images/plastic.svg',
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          applicationColumn(
                            label: 'Large Appliances',
                            image: 'images/large_appliances.svg',
                          ),
                          applicationColumn(
                            label: 'Metal',
                            image: 'images/iron.svg',
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          applicationColumn(
                            label: 'Small Appliances',
                            image: 'images/home_appliances.svg',
                          ),
                          applicationColumn(
                            label: 'Paper',
                            image: 'images/paper.svg',
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          applicationColumn(
                            label: 'Communications',
                            image: 'images/communication.svg',
                          ),
                          InkWell(
                            child: applicationColumn(
                              label: 'See All',
                              image: 'images/all_services.svg',
                            ),
                            onTap: () {
                              allServices(context);
                            },
                          ),
                        ],
                      ),
                    ],
                  ),*/
                      GridView.count(
                    crossAxisCount: 4,
                    children: List.generate(categoryList.length, (index) {
                      return Center(
                        child: InkWell(
                          onTap: () {
                            if (categoryList[index].text == "See All") {
                              allServices(context);
                            } else {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ProductCategoryPage(
                                          title: categoryList[index].text!,
                                          productsList: categoryList[index]
                                              .productsList)));
                            }
                          },
                          child: applicationColumnNetwork(
                            label: categoryList[index].text,
                            image: (url + categoryList[index].imageUrl!),
                          ),
                        ),
                      );
                    }),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void allServices(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => const AllServicesNewPage()));
  }

  void getCategoryList() async {
    await LoaderUtils().showAlertDialog(context);
    try {
      var dio = Dio();
      var response = await dio.get(productCategoryUrl);
      if (kDebugMode) {
        print(response.data);
        print(response.data["data"]["content"]);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          if (response.data["data"]["content"] != null) {
            var rest = response.data["data"]["content"];

            List<ProductCategoryModel> list = rest
                .map<ProductCategoryModel>(
                    (json) => ProductCategoryModel.fromJson(json))
                .toList();
            if (list.length > 7) {
              Iterable<ProductCategoryModel> list1 = list.take(7);
              if (kDebugMode) {
                print("list size ${list1.length}");
              }

              ProductCategoryModel productCategoryModel =
                  ProductCategoryModel();
              ProductModel productModel = ProductModel();
              List<ProductModel> pList = [];
              productCategoryModel.text = "See All";
              productCategoryModel.id = DateTime.now().microsecondsSinceEpoch;
              productCategoryModel.imageUrl = "";
              pList.add(productModel);
              productCategoryModel.productsList = pList;

              categoryList.addAll(list1);
              categoryList.add(productCategoryModel);
            } else {
              categoryList.addAll(list);
            }
            if (kDebugMode) {
              print("categoryList size ${categoryList.length}");
            }
            setState(() {});
          } else {
            message("No product found.", context);
          }
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      message(e.toString(), context);
      setState(() {});
    }
  }
}
