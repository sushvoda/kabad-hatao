import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:kabadhatao/utils/preference.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ColorFile.dart';
import 'Common.dart';
import 'Images.dart';

class PersonalInformationPage extends StatefulWidget {
  const PersonalInformationPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PersonalInformationPageWidgetState();
}

class _PersonalInformationPageWidgetState
    extends State<PersonalInformationPage> {
  String userName = "";
  String userMobile = "";
  String email = "";
  String profilePic = "";
  int userId = 0;

  dynamic imageFile;

  TextEditingController customerController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailAddressController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  FocusNode customerID = FocusNode();
  FocusNode fullName = FocusNode();
  FocusNode emailAddress = FocusNode();
  FocusNode phoneNo = FocusNode();
  FocusNode address = FocusNode();

  @override
  void initState() {
    getValues();
    super.initState();
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userName = sharedPreferences.getString("name") ?? "";
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    email = sharedPreferences.getString("email") ?? "";
    profilePic = sharedPreferences.getString("profilePic") ?? "";
    userId = sharedPreferences.getInt("id") ?? 0;

    customerController.text = userId.toString();
    fullNameController.text = userName;
    phoneNoController.text = userMobile;
    emailAddressController.text = email;

    getUserProfile();

    setState(() {});
  }

  @override
  void dispose() {
    customerID.dispose();
    fullName.dispose();
    emailAddress.dispose();
    phoneNo.dispose();
    address.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grayBG,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Personal Information'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 20.0),
        child: Column(
          children: [
            Container(
              child: Stack(
                alignment: Alignment.bottomRight,
                children: [
                  InkWell(
                    onTap: () => _moreOptionsBottomSheetMenu(context),
                    child: CircleAvatar(
                      backgroundColor: grayLiteBG,
                      radius: 60,
                      child: imageFile == null && profilePic.isEmpty
                          ? Padding(
                              padding: const EdgeInsets.all(30),
                              child: svgUserIcon,
                            )
                          : imageFile != null
                              ? ClipOval(
                                  child: Image.file(
                                    imageFile,
                                    fit: BoxFit.fitWidth,
                                    width: 120.0,
                                    height: 120.0,
                                  ),
                                )
                              : profilePic.isEmpty
                                  ? Padding(
                                      padding: const EdgeInsets.all(30),
                                      child: svgUserIcon,
                                    )
                                  : ClipOval(
                                      child: Image.network(
                                        url + profilePic,
                                        fit: BoxFit.cover,
                                        width: 120.0,
                                        height: 120.0,
                                      ),
                                    ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(5.0),
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 12,
                      child: Icon(
                        Icons.camera_alt,
                        color: primaryColor,
                        size: 12,
                      ), //Text
                    ),
                  ),
                ],
              ),
              height: 120.0,
              width: 120.0,
              alignment: Alignment.center,
            ),
            makeText(
                fontWeight: FontWeight.normal,
                color: grayColor,
                label: "Update Photo",
                alignment: Alignment.center),
            makeInput(
                label: 'Customer Id',
                obscureText: false,
                controller: customerController,
                focusNode: customerID,
                enabled: false,
                hintText: ''),
            makeInput(
                label: 'Full Name',
                obscureText: false,
                controller: fullNameController,
                focusNode: fullName,
                enabled: true,
                hintText: 'Your Name'),
            makeInput(
                label: 'Email Address',
                obscureText: false,
                controller: emailAddressController,
                focusNode: emailAddress,
                enabled: true,
                hintText: 'example@gmail.com'),
            makeInput(
                label: 'Phone No.',
                obscureText: false,
                controller: phoneNoController,
                focusNode: phoneNo,
                enabled: false,
                hintText: 'Your Number'),
            GestureDetector(
              onTap: () {
                addressesScreen(context);
              },
              child: makeInput(
                  label: 'Address',
                  obscureText: false,
                  controller: addressController,
                  focusNode: address,
                  enabled: false,
                  hintText: 'Enter your address'),
            ),
            Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              elevation: 10.0,
              color: primaryColor,
              clipBehavior: Clip.antiAlias,
              child: MaterialButton(
                padding: const EdgeInsets.all(15),
                child: const Text("Update Now",
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                onPressed: () {
                  _asyncFileUpload();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  _moreOptionsBottomSheetMenu(context) {
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return Container(
          color: Colors.transparent,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: ListBody(
              children: <Widget>[
                makeText(
                    label: 'Choose an action',
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
                const SizedBox(height: 20),
                GestureDetector(
                  child: const Text('Take a picture'),
                  onTap: () {
                    Navigator.pop(context);
                    _getFromCamera();
                  },
                ),
                const SizedBox(height: 20),
                GestureDetector(
                  child: const Text('Select from gallery'),
                  onTap: () {
                    Navigator.pop(context);
                    _getFromGallery();
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  /// Get from gallery
  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      imageQuality: 25,
      maxWidth: MediaQuery.of(context).size.width,
      maxHeight: 400,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  /// Get from Camera
  _getFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      imageQuality: 25,
      maxWidth: MediaQuery.of(context).size.width,
      maxHeight: 400,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  void getUserProfile() async {
    try {
      var dio = Dio();

      if (kDebugMode) {
        print(getUserProfileUrl + "/$userId");
      }
      await LoaderUtils().showAlertDialog(context);

      var response = await dio.get(getUserProfileUrl + "/$userId");
      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          setDetails(response.data['data']['user_profile']);
          //Preference().setUserProfile(response.data['data']['userProfile']);
          setState(() {});
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      message(e.toString(), context);
      setState(() {});
    }
  }

  _asyncFileUpload() async {
    try {
      //create multipart request for POST or PATCH method
      var request = http.MultipartRequest("POST", Uri.parse(getUserProfileUrl));
      //add text fields
      request.fields["id"] = userId.toString();
      request.fields["name"] = fullNameController.text.toString();
      request.fields["email"] = emailAddressController.text.toString();
      if (imageFile != null) {
        //create multipart using filepath, string or bytes
        var pic = await http.MultipartFile.fromPath("image", imageFile.path);
        //add multipart to request
        request.files.add(pic);
      }
      await LoaderUtils().showAlertDialog(context);
      var response = await request.send();
      Navigator.pop(context);

      var responseString = await response.stream.bytesToString();
      final decodedMap = json.decode(responseString);
      if (kDebugMode) {
        print(responseString);
      }
      if (response.statusCode == 200) {
        Preference().setUserProfile(decodedMap['data']['userProfile']);
      } else {
        message(decodedMap['message'], context);
      }
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      message(e.toString(), context);
      setState(() {});
    }
  }

  void setDetails(data) {
    if (kDebugMode) {
      print(data);
    }
    userName = data['name'] ?? "";
    email = data['email'] ?? "";
    profilePic = data['profilePic'] ?? "";

    fullNameController.text = userName;
    emailAddressController.text = email;

    Preference().setEmail(email);
    Preference().setName(userName);
    Preference().setProfilePic(profilePic);
  }
}
