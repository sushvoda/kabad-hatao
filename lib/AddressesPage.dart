import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/Common.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AddressPage.dart';
import 'ColorFile.dart';
import 'model/models.dart';

class AddressesPage extends StatefulWidget {
  const AddressesPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddressesPageWidgetState();
}

class _AddressesPageWidgetState extends State<AddressesPage> {
  int userId = 0;

  List<AddressModel> addressList = [];

  late AddressModel selectedAddress;

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getInt("id") ?? 0;

    Future.delayed(Duration.zero, () {
      getAddressesList();
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    getValues();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Addresses'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Builder(
        builder: (context) => SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: lightGreenColor,
                    borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                    border: Border.all(color: primaryColor),
                  ),
                  child: GestureDetector(
                    child: const Padding(
                      padding: EdgeInsets.all(30.0),
                      child: SizedBox(
                        width: double.infinity,
                        child: Text(
                          "Add New Address",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.normal,
                              color: Colors.black),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      openAddressScreen(context, AddressModel());
                    },
                  ),
                ),
              ),
              ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: addressList.length,
                  itemBuilder: (context, position) {
                    return Card(
                      margin: const EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                      elevation: 5.0,
                      child: SizedBox(
                        width: double.infinity,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(5.0)),
                            border: Border.all(color: Colors.grey),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  addressList[position].fullName ?? "",
                                  style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                                const SizedBox(height: 5),
                                Text(
                                  (addressList[position].houseName ?? "") +
                                      ", " +
                                      (addressList[position].pinCode == null
                                          ? ""
                                          : addressList[position]
                                              .pinCode
                                              .toString()),
                                  style: const TextStyle(fontSize: 12),
                                ),
                                Text(
                                  addressList[position].landmark ?? "",
                                  style: const TextStyle(fontSize: 12),
                                ),
                                const SizedBox(height: 10),
                                Text(
                                  "Phone Number : " +
                                      (addressList[position].phoneNo ?? ""),
                                  style: const TextStyle(fontSize: 12),
                                ),
                                const SizedBox(height: 10),
                                Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        openAddressScreen(
                                            context, addressList[position]);
                                      },
                                      child: ConstrainedBox(
                                        constraints: const BoxConstraints(
                                            minWidth: 100.0),
                                        child: DecoratedBox(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(5.0)),
                                            border:
                                                Border.all(color: Colors.grey),
                                          ),
                                          child: const Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Text(
                                              "Edit",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.black),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    const Spacer(),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pop(
                                            context, addressList[position]);
                                      },
                                      child: DecoratedBox(
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(5.0)),
                                          border:
                                              Border.all(color: Colors.grey),
                                        ),
                                        child: const Padding(
                                          padding: EdgeInsets.all(10.0),
                                          child: Text(
                                            "Use this Address",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.normal,
                                                color: Colors.black),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  })
            ],
          ),
          /*slivers: <Widget>[
            const SliverToBoxAdapter(),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, position) {
                  return Card(
                    margin: const EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                    elevation: 5.0,
                    child: SizedBox(
                      width: double.infinity,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(5.0)),
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                addressList[position].fullName ?? "",
                                style: const TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                (addressList[position].houseName ?? "") +
                                    ", " +
                                    (addressList[position].pinCode == null
                                        ? ""
                                        : addressList[position]
                                            .pinCode
                                            .toString()),
                                style: const TextStyle(fontSize: 12),
                              ),
                              Text(
                                addressList[position].landmark ?? "",
                                style: const TextStyle(fontSize: 12),
                              ),
                              const SizedBox(height: 10),
                              Text(
                                "Phone Number : " +
                                    (addressList[position].phoneNo ?? ""),
                                style: const TextStyle(fontSize: 12),
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  ConstrainedBox(
                                    constraints:
                                        const BoxConstraints(minWidth: 100.0),
                                    child: DecoratedBox(
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(5.0)),
                                        border: Border.all(color: Colors.grey),
                                      ),
                                      child: const Padding(
                                        padding: EdgeInsets.all(10.0),
                                        child: Text(
                                          "Edit",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.black),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  DecoratedBox(
                                    decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(5.0)),
                                      border: Border.all(color: Colors.grey),
                                    ),
                                    child: const Padding(
                                      padding: EdgeInsets.all(10.0),
                                      child: Text(
                                        "Use this Address",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.black),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                childCount: addressList == null ? 0 : addressList.length,
              ),
            ),
          ],*/
        ),
      ),
    );
  }

  void getAddressesList() async {
    await LoaderUtils().showAlertDialog(context);
    try {
      var dio = Dio();
      var url = getUserProfileUrl + "/$userId";
      if (kDebugMode) {
        print(url);
      }
      var response = await dio.get(url);
      if (kDebugMode) {
        print(response.data);
        print(response.data["data"]["user_profile"]);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          if (response.data["data"]["user_profile"]["addressses"] != null) {
            var rest =
                response.data["data"]["user_profile"]["addressses"] as List;

            List<AddressModel> list = rest
                .map<AddressModel>((json) => AddressModel.fromJson(json))
                .toList();
            addressList.addAll(list);
            setState(() {});
          } else {
            message("No address found.", context);
          }
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }

  void openAddressScreen(
      BuildContext context, AddressModel addressModel) async {
    var address = await Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) =>  AddressPage(addressModel)));
    if (address != null) {
      if (kDebugMode) {
        print(address);
      }
      getAddressesList();
    } else {
      if (kDebugMode) {
        print("address null");
      }
    }
  }
}
