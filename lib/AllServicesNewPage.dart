import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/Common.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ProductCategoryPage.dart';
import 'model/models.dart';

class AllServicesNewPage extends StatefulWidget {
  const AllServicesNewPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AllServicesNewPageWidgetState();
}

class _AllServicesNewPageWidgetState extends State<AllServicesNewPage> {
  int userId = 0;

  List<ProductCategoryModel> categoryList = [];

  late ProductCategoryModel selectedCategory;

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getInt("id") ?? 0;

    Future.delayed(Duration.zero, () {
      getCategoryList();
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    getValues();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Category List'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Builder(
        builder: (context) => Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: GridView.count(
                crossAxisCount: 3,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 4.0,
                children: List.generate(categoryList.length, (index) {
                  return Center(
                    child: Card(
                      elevation: 5.0,
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  ProductCategoryPage(
                                      title: categoryList[index].text!,
                                      productsList:
                                          categoryList[index].productsList)));
                        },
                        child: applicationColumnNetwork(
                          label: categoryList[index].text,
                          image: (url + categoryList[index].imageUrl!),
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ),
            Visibility(
                child: const Center(
                  child: Text(
                    "No Product Found",
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
                visible: categoryList.isEmpty),
          ],
        ),
      ),
    );
  }

  void getCategoryList() async {
    await LoaderUtils().showAlertDialog(context);
    try {
      var dio = Dio();
      var response = await dio.get(productCategoryUrl);
      if (kDebugMode) {
        print(response.data);
        print(response.data["data"]["content"]);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          if (response.data["data"]["content"] != null) {
            var rest = response.data["data"]["content"];

            List<ProductCategoryModel> list = rest
                .map<ProductCategoryModel>(
                    (json) => ProductCategoryModel.fromJson(json))
                .toList();
            categoryList.addAll(list);
            if (kDebugMode) {
              print("categoryList size ${categoryList.length}");
            }
            setState(() {});
          } else {
            message("No product found.", context);
          }
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      message(e.toString(), context);
      setState(() {});
    }
  }
}
