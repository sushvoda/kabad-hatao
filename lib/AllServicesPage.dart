import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/Common.dart';

class AllServicesPage extends StatefulWidget {
  const AllServicesPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AllServicesPageWidgetState();
}

class _AllServicesPageWidgetState extends State<AllServicesPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: makeText(label: 'All Services'),
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_outlined,
              size: 20,
              color: Colors.black87,
            ),
          ),
          systemOverlayStyle: SystemUiOverlayStyle.dark,
        ),
        body: Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Column(children: [
              Flexible(
                child: ListView(
                    padding: const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: [
                      Column(
                        children: [
                          Card(
                            margin: const EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                                top: 10.0,
                                bottom: 5.0),
                            elevation: 5.0,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  const SizedBox(height: 5.0),
                                  makeText(
                                    label: 'Computer',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  const SizedBox(height: 10.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () {

                                            },
                                            child: applicationColumn(
                                              label: 'Laptop',
                                              image: 'images/laptop.svg',
                                            ),
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'Router',
                                            image: 'images/router.svg',
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          applicationColumn(
                                            label: 'Printer',
                                            image: 'images/printer.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'UPS',
                                            image: 'images/ups.svg',
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          applicationColumn(
                                            label: 'Tablet',
                                            image: 'images/tablet.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'Desktop',
                                            image: 'images/desktop.svg',
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            margin: const EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                                top: 10.0,
                                bottom: 5.0),
                            elevation: 5.0,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  const SizedBox(height: 5.0),
                                  makeText(
                                    label: 'Large Appliances',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  const SizedBox(height: 10.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          applicationColumn(
                                            label: 'Refrigerators',
                                            image: 'images/refrigerators.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'Television',
                                            image: 'images/television.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'Split Ac',
                                            image: 'images/split_ac.svg',
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          applicationColumn(
                                            label: 'Microwave',
                                            image:
                                                'images/bult_in_microwave.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'Dish Washer',
                                            image: 'images/dish_washer.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label:
                                                'Washing Machine Fully Automatic',
                                            image:
                                                'images/washing_machine_fully_automatic.svg',
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          applicationColumn(
                                            label: 'Dryer',
                                            image: 'images/dryer.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'Inverter Split Ac',
                                            image:
                                                'images/inverter_split_ac.svg',
                                          ),
                                          const SizedBox(height: 15.0),
                                          applicationColumn(
                                            label: 'Window Ac 1 Ton',
                                            image: 'images/window_ac1ton.svg',
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            margin: const EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                                top: 10.0,
                                bottom: 5.0),
                            elevation: 5.0,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  const SizedBox(height: 5.0),
                                  makeText(
                                    label: 'Small Appliances',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  const SizedBox(height: 10.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      applicationColumn(
                                        label: 'Cooler',
                                        image: 'images/cooler.svg',
                                      ),
                                      applicationColumn(
                                        label: 'CRT Television',
                                        image: 'images/crt_television.svg',
                                      ),
                                      applicationColumn(
                                        label: 'Microwaves',
                                        image: 'images/microwaves.svg',
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            margin: const EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                                top: 10.0,
                                bottom: 5.0),
                            elevation: 5.0,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  const SizedBox(height: 5.0),
                                  makeText(
                                    label: 'Communication',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  const SizedBox(height: 10.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      applicationColumn(
                                        label: 'Mobile',
                                        image: 'images/communication.svg',
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            margin: const EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                                top: 10.0,
                                bottom: 5.0),
                            elevation: 5.0,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  const SizedBox(height: 5.0),
                                  makeText(
                                    label: 'Plastic',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  const SizedBox(height: 10.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      applicationColumn(
                                        label: 'Cardboard',
                                        image: 'images/cardboard.svg',
                                      ),
                                      applicationColumn(
                                        label: 'Plastic Bag',
                                        image: 'images/plastic_bag.svg',
                                      ),
                                      applicationColumn(
                                        label: 'Plastic',
                                        image: 'images/plastic.svg',
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            margin: const EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                                top: 10.0,
                                bottom: 5.0),
                            elevation: 5.0,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  const SizedBox(height: 5.0),
                                  makeText(
                                    label: 'Metal',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  const SizedBox(height: 10.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      applicationColumn(
                                        label: 'Cooler',
                                        image: 'images/cooler.svg',
                                      ),
                                      applicationColumn(
                                        label: 'CRT Television',
                                        image: 'images/crt_television.svg',
                                      ),
                                      applicationColumn(
                                        label: 'Microwaves',
                                        image: 'images/microwaves.svg',
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            margin: const EdgeInsets.only(
                                left: 10.0,
                                right: 10.0,
                                top: 10.0,
                                bottom: 5.0),
                            elevation: 5.0,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  const SizedBox(height: 5.0),
                                  makeText(
                                    label: 'Paper',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                  const SizedBox(height: 10.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      applicationColumn(
                                        label: 'Books',
                                        image: 'images/books.svg',
                                      ),
                                      applicationColumn(
                                        label: 'Empty Cartons',
                                        image: 'images/empty_cartons.svg',
                                      ),
                                      applicationColumn(
                                        label: 'Paper',
                                        image: 'images/paper.svg',
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]),
              )
            ])));
  }
}
