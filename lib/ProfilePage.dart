import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:kabadhatao/Drawer.dart';
import 'package:kabadhatao/utils/NetworkClass.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:kabadhatao/utils/preference.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ColorFile.dart';
import 'Common.dart';
import 'Images.dart';
import 'StringFile.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfilePageWidgetState();
}

class _ProfilePageWidgetState extends State<ProfilePage> {
  int userId = 0;
  String userName = "";
  String userMobile = "";
  String userEmail = "";
  String profilePic = "";
  bool isPartner = false;
  bool isLoggedIn = false;
  var imageFile;

  TextEditingController customerController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailAddressController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  FocusNode customerID = FocusNode();
  FocusNode fullName = FocusNode();
  FocusNode emailAddress = FocusNode();
  FocusNode phoneNo = FocusNode();
  FocusNode address = FocusNode();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    getValues();
    super.initState();
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getInt("id") ?? 0;
    userName = sharedPreferences.getString("name") ?? kabadHatao;
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    userEmail = sharedPreferences.getString("email") ?? "";
    profilePic = sharedPreferences.getString("profilePic") ?? "";
    isLoggedIn = sharedPreferences.getBool("is_logged_in") ?? false;

    customerController.text = userId.toString();
    fullNameController.text = userName;
    phoneNoController.text = userMobile;
    emailAddressController.text = userEmail;

    if (kDebugMode) {
      print("userId : " + userId.toString());
      print("userMobile : " + userMobile);
      print("profilePic : " + url + profilePic);
    }

    getUserProfile();

    setState(() {});
  }

  @override
  void dispose() {
    customerController.dispose();
    fullNameController.dispose();
    emailAddressController.dispose();
    phoneNoController.dispose();
    addressController.dispose();
    customerID.dispose();
    fullName.dispose();
    emailAddress.dispose();
    phoneNo.dispose();
    address.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grayBG,
      resizeToAvoidBottomInset: true,
      key: _scaffoldKey,
      drawer: drawer(
        context: context,
        userName: userName,
        userMobile: userMobile,
        profilePic: profilePic,
        isPartner: false,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery
            .of(context)
            .viewPadding
            .top),
        child: Column(
          children: [
            ColoredBox(
              color: Colors.white,
              child: Row(
                children: [
                  IconButton(
                    icon: svgDrawer,
                    onPressed: () => _scaffoldKey.currentState?.openDrawer(),
                  ),
                  logoKH(logoKHImage),
                  const Spacer(flex: 1),
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    child: OutlinedButton(
                      onPressed: () {
                        Preference().clearPreference();
                        loginScreen(context);
                      },
                      style: OutlinedButton.styleFrom(
                        side: BorderSide(
                          color: userName.isNotEmpty
                              ? textColorLite
                              : primaryColor,
                        ),
                      ),
                      child: makeText(
                        label: isLoggedIn ? "Logout" : "Login",
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            isLoggedIn
                ? Flexible(
              child: ListView(
                padding:
                const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                children: [
                  Container(
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        InkWell(
                          onTap: () =>
                              _moreOptionsBottomSheetMenu(context),
                          child: CircleAvatar(
                            backgroundColor: grayLiteBG,
                            radius: 60,
                            child: imageFile == null && profilePic.isEmpty
                                ? Padding(
                              padding: const EdgeInsets.all(30),
                              child: svgUserIcon,
                            )
                                : imageFile != null
                                ? ClipOval(
                              child: Image.file(
                                imageFile,
                                fit: BoxFit.fitWidth,
                                width: 120.0,
                                height: 120.0,
                              ),
                            )
                                : profilePic.isEmpty
                                ? Padding(
                              padding:
                              const EdgeInsets.all(30),
                              child: svgUserIcon,
                            )
                                : ClipOval(
                              child: Image.network(
                                url + profilePic,
                                fit: BoxFit.cover,
                                width: 120.0,
                                height: 120.0,
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.all(5.0),
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 12,
                            child: Icon(
                              Icons.camera_alt,
                              color: primaryColor,
                              size: 12,
                            ), //Text
                          ),
                        ),
                      ],
                    ),
                    height: 120.0,
                    width: 120.0,
                    alignment: Alignment.center,
                  ),
                  makeText(
                      fontWeight: FontWeight.normal,
                      color: grayColor,
                      label: "Update Photo",
                      alignment: Alignment.center),
                  makeInput(
                      label: 'Customer Id',
                      obscureText: false,
                      controller: customerController,
                      focusNode: customerID,
                      enabled: false,
                      hintText: ''),
                  makeInput(
                      label: 'Full Name',
                      obscureText: false,
                      controller: fullNameController,
                      focusNode: fullName,
                      enabled: true,
                      hintText: 'Your Name'),
                  makeInput(
                      label: 'Email Address',
                      obscureText: false,
                      controller: emailAddressController,
                      focusNode: emailAddress,
                      enabled: true,
                      hintText: 'example@gmail.com'),
                  makeInput(
                      label: 'Phone No.',
                      obscureText: false,
                      controller: phoneNoController,
                      focusNode: phoneNo,
                      enabled: true,
                      hintText: 'Your Phone Number'),
                  GestureDetector(
                    onTap: () {
                      addressesScreen(context);
                    },
                    child: makeInput(
                        label: 'Address',
                        obscureText: false,
                        controller: addressController,
                        focusNode: address,
                        enabled: false,
                        hintText: 'Enter your address'),
                  ),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    elevation: 10.0,
                    color: primaryColor,
                    clipBehavior: Clip.antiAlias,
                    child: MaterialButton(
                      padding: const EdgeInsets.all(15),
                      child: const Text("Update Now",
                          style: TextStyle(
                              fontSize: 16.0, color: Colors.white)),
                      onPressed: () {
                        uploadDocument(context, imageFile);
                      },
                    ),
                  ),
                ],
              ),
            )
                : Flexible(
              child: ListView(
                padding: const EdgeInsets.all(10.0),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                children: [
                  Card(
                    child: ListTile(
                      contentPadding: const EdgeInsets.all(10.0),
                      dense: true,
                      leading: SvgPicture.asset(
                        'images/price_list.svg',
                        color: primaryColor,
                        width: 25.0,
                        height: 25.0,
                      ),
                      title: const Text('Become a partner'),
                      subtitle: makeText(
                          label:
                          'You can switch to partner account & earn money',
                          fontSize: 10.0),
                      trailing: Switch(
                        value: isPartner,
                        onChanged: (bool value) {
                          isPartner = value;
                        },
                        activeColor: primaryColor,
                      ),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      contentPadding: const EdgeInsets.all(10.0),
                      dense: true,
                      leading: SvgPicture.asset(
                        'images/price_list.svg',
                        color: primaryColor,
                        width: 25.0,
                        height: 25.0,
                      ),
                      title: const Text('About Kabad Hatao Company'),
                      subtitle: makeText(
                          label: 'You can find more information here',
                          fontSize: 10.0),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      contentPadding: const EdgeInsets.all(10.0),
                      dense: true,
                      leading: SvgPicture.asset(
                        'images/price_list.svg',
                        color: primaryColor,
                        width: 25.0,
                        height: 25.0,
                      ),
                      title: const Text('Share Kabad Hatao Company'),
                      subtitle: makeText(
                          label:
                          'You can share with your family & friends circle',
                          fontSize: 10.0),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _moreOptionsBottomSheetMenu(context) {
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return Container(
          color: Colors.transparent,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: ListBody(
              children: <Widget>[
                makeText(
                    label: 'Choose an action',
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
                const SizedBox(height: 20),
                GestureDetector(
                  child: const Text('Take a picture'),
                  onTap: () {
                    Navigator.pop(context);
                    _getFromCamera();
                  },
                ),
                const SizedBox(height: 20),
                GestureDetector(
                  child: const Text('Select from gallery'),
                  onTap: () {
                    Navigator.pop(context);
                    _getFromGallery();
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  /// Get from gallery
  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      imageQuality: 25,
      maxWidth: MediaQuery
          .of(context)
          .size
          .width,
      maxHeight: 400,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  /// Get from Camera
  _getFromCamera() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      imageQuality: 25,
      maxWidth: MediaQuery
          .of(context)
          .size
          .width,
      maxHeight: 400,
    );
    if (pickedFile != null) {
      setState(() {
        imageFile = File(pickedFile.path);
      });
    }
  }

  void getUserProfile() async {
    try {
      var dio = Dio();

      if (kDebugMode) {
        print(getUserProfileUrl + "/$userId");
      }
      await LoaderUtils().showAlertDialog(context);

      var response = await dio.get(getUserProfileUrl + "/$userId");
      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          setDetails(response.data['data']['user_profile']);
          //Preference().setUserProfile(response.data['data']['userProfile']);
          setState(() {});
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      message(e.toString(), context);
      setState(() {});
    }
  }

  _asyncFileUpload() async {
    try {
      //create multipart request for POST or PATCH method
      var request = http.MultipartRequest("POST", Uri.parse(getUserProfileUrl));
      //add text fields
      request.fields["id"] = userId.toString();
      request.fields["name"] = fullNameController.text.toString();
      request.fields["email"] = emailAddressController.text.toString();
      if (imageFile != null) {
        //create multipart using filepath, string or bytes
        var pic = await http.MultipartFile.fromPath("image", imageFile.path);
        //add multipart to request
        request.files.add(pic);
      }
      await LoaderUtils().showAlertDialog(context);
      var response = await request.send();

      //Get the response from the server
      /* var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      if (kDebugMode) {
        print(responseString);
      }*/
      Navigator.pop(context);

      var responseString = await response.stream.bytesToString();
      final decodedMap = json.decode(responseString);
      if (kDebugMode) {
        print(responseString);
      }
      if (response.statusCode == 200) {
        Preference().setUserProfile(decodedMap['data']['userProfile']);
      } else {
        message(decodedMap['message'], context);
      }
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      message(e.toString(), context);
      setState(() {});
    }
  }

  void setDetails(data) {
    if (kDebugMode) {
      print(data);
    }
    userName = data['name'] ?? "";
    userEmail = data['email'] ?? "";
    profilePic = data['profilePic'] ?? "";

    fullNameController.text = userName;
    emailAddressController.text = userEmail;

    Preference().setEmail(userEmail);
    Preference().setName(userName);
    Preference().setProfilePic(profilePic);
  }

  Future<void> uploadDocument(BuildContext context, File file) async {
    await LoaderUtils().showAlertDialog(context);
    setState(() {});
    if (kDebugMode) {
      print(getUserProfileUrl);
      print(userId);
    }
    NetworkClass()
        .dio_post_form_data(context, getUserProfileUrl, file, userId)
        .then((value) {
      Navigator.pop(context);
      if (kDebugMode) {
        print("document upload response : comes");
      }
      if (value != null) {
        Response response = value;
        if (response.statusCode == 200) {
          if (kDebugMode) {
            print("response : " + response.data.toString());
          }
          Object intervention = response.data;
          if (intervention is String) {
            if (kDebugMode) {
              print(response.data.toString().toLowerCase());
            }
          } else {
            if (kDebugMode) {
              print("status message array : " + response.data.toString());
            }
          }
        } else {
          if (kDebugMode) {
            print("status code : " + response.statusCode.toString());
          }
        }
      } else {
        if (kDebugMode) {
          print("response null");
        }
      }
      setState(() {});
    });
  }
}
