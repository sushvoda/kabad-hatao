import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/Common.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ColorFile.dart';
import 'model/models.dart';

class ProductCategoryPage extends StatefulWidget {
  List<ProductModel>? productsList;
  String title;

  ProductCategoryPage(
      {Key? key, required this.title, required this.productsList})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProductCategoryPageWidgetState();
}

class _ProductCategoryPageWidgetState extends State<ProductCategoryPage> {
  int userId = 0;

  late ProductModel selectedProduct;

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getInt("id") ?? 0;

    if (kDebugMode) {
      print("productsList size : ${widget.productsList?.length}");
    }
  }

  @override
  void initState() {
    super.initState();
    getValues();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: widget.title),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Builder(
        builder: (context) => Stack(
          children: [
            SingleChildScrollView(
              physics: const ScrollPhysics(),
              child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: widget.productsList!.length,
                itemBuilder: (context, position) {
                  return Card(
                    margin: const EdgeInsets.only(
                        left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                    elevation: 5.0,
                    child: SizedBox(
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(5.0)),
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FadeInImage.assetNetwork(
                              placeholder: "images/loading.gif",
                              image: url +
                                  widget.productsList![position].imageUrl!,
                              width: MediaQuery.of(context).size.width - 20,
                              imageErrorBuilder: (context, error, stackTrace) {
                                return const Image(
                                    height: 200,
                                    image:
                                        AssetImage('images/placeholder.png'));
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 10, right: 10, top: 10),
                              child: Row(
                                children: [
                                  Text(
                                    widget.productsList![position].description!,
                                    style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  const Spacer(),
                                  InkWell(
                                    onTap: () => {},
                                    child: const DecoratedBox(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5.0)),
                                          color: primaryColor),
                                      child: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(15, 8.0, 15, 8),
                                        child: Text(
                                          "Add    +",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const Divider(
                                thickness: 1, indent: 10, endIndent: 10),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Row(
                                children: [
                                  Text(
                                    "Product ID : ${DateTime.now().microsecondsSinceEpoch.toString()}",
                                    style: const TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                  const Spacer(),
                                  const Text(
                                    "Bulk Inquiry : ",
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () => {},
                                    child: const Text(
                                      "Here",
                                      style: TextStyle(
                                        color: primaryColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 10, left: 10, right: 10),
                              child: Row(
                                children: [
                                  const Text(
                                    "Price:",
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  const Text(
                                    " ₹",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: primaryColor),
                                  ),
                                  const Text(
                                    "8/kg",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.black),
                                  ),
                                  const Spacer(),
                                  const Text(
                                    "Value:",
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  const Text(
                                    " ₹",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: primaryColor),
                                  ),
                                  Text(
                                    widget.productsList![position].cost!
                                        .toString(),
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.black),
                                  ),
                                ],
                              ),
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 10, left: 10, right: 10),
                              child: Text(
                                "100% Copper Condenser with extended lifespan to ensure that the air conditioner is protected in all weather conditions",
                                style: TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, bottom: 10),
                              child: Text(
                                "(T&C)",
                                style: TextStyle(
                                    fontSize: 12, color: primaryColor),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    decoration: const BoxDecoration(
                                        color: Colors.black54),
                                    child: const Text(
                                      "Pickup Request",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 1),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    decoration: const BoxDecoration(
                                        color: primaryColor),
                                    child: const Text(
                                      "Add for Pickup",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            Visibility(
                child: const Center(
                  child: Text(
                    "No Product Found",
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
                visible: widget.productsList!.isEmpty),
          ],
        ),
      ),
    );
  }
}
