import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kabadhatao/ColorFile.dart';
import 'package:kabadhatao/Images.dart';
import 'package:kabadhatao/model/models.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Common.dart';

class AddressPage extends StatefulWidget {
  AddressModel addressModel;

  AddressPage(this.addressModel, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddressPageWidgetState();
}

class _AddressPageWidgetState extends State<AddressPage> {
  String userName = "";
  String userMobile = "";
  int userId = 0;
  int addressId = 0;

  String addressValue = "";

  TextEditingController fullNameController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController pincodeController = TextEditingController();
  TextEditingController houseController = TextEditingController();
  TextEditingController landMarkController = TextEditingController();

  FocusNode fullName = FocusNode();
  FocusNode phoneNo = FocusNode();
  FocusNode address = FocusNode();
  FocusNode pincode = FocusNode();
  FocusNode houseNo = FocusNode();
  FocusNode landMark = FocusNode();

  @override
  void initState() {
    getValues();
    super.initState();
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userName = sharedPreferences.getString("name") ?? "";
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    userId = sharedPreferences.getInt("id") ?? 0;

    fullNameController.text = userName;
    phoneNoController.text = userMobile;

    if (widget.addressModel.id != null || widget.addressModel.id != 0) {
      addressId = widget.addressModel.id!;
    }
    if (widget.addressModel.fullName!.isNotEmpty) {
      fullNameController.text = widget.addressModel.fullName!;
    }
    if (widget.addressModel.phoneNo!.isNotEmpty) {
      phoneNoController.text = widget.addressModel.phoneNo!;
    }
    if (widget.addressModel.houseName!.isNotEmpty) {
      houseController.text = widget.addressModel.houseName!;
    }
    if (widget.addressModel.landmark!.isNotEmpty) {
      landMarkController.text = widget.addressModel.landmark!.toUpperCase();
    }
    if (widget.addressModel.houseType!.isNotEmpty) {
      addressValue = widget.addressModel.houseType!.toLowerCase() == 'home'
          ? 'Home'
          : 'Work';
    }
    pincodeController.text = widget.addressModel.pinCode.toString();

    setState(() {});
  }

  @override
  void dispose() {
    fullName.dispose();
    phoneNo.dispose();
    address.dispose();
    pincode.dispose();
    houseNo.dispose();
    landMark.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Addresses'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).viewPadding.top),
        child: ListView(
          padding: const EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 20.0),
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          children: [
            makeInputWithBoarder(
                label: 'Full Name (Required)*',
                controller: fullNameController,
                focusNode: fullName,
                obscureText: false,
                enabled: true,
                hintText: 'Your Name'),
            makeInputNumberWithBoarder(
                label: 'Phone No. (Required)*',
                controller: phoneNoController,
                focusNode: phoneNo,
                hintText: 'Your Number',
                maxLength: 10,
                heightSizedBox: 0.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: makeInputNumberWithBoarder(
                      label: 'Pincode',
                      controller: pincodeController,
                      focusNode: pincode,
                      hintText: '',
                      maxLength: 6,
                      heightSizedBox: 0.0),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Center(
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: primaryColor,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5.0)),
                        border: Border.all(color: Colors.grey),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: [
                            svgLocation,
                            const SizedBox(width: 5),
                            const Text(
                              'Use my location',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            makeInputWithBoarder(
                label: 'House No.Building Name (Required)*',
                controller: houseController,
                focusNode: houseNo,
                obscureText: false,
                enabled: true,
                hintText: ''),
            makeInputWithBoarder(
                label: 'Landmark (Required)*',
                controller: landMarkController,
                focusNode: landMark,
                obscureText: false,
                enabled: true,
                hintText: ''),
            makeText(label: 'Type of address'),
            const SizedBox(height: 5.0),
            Row(
              children: [
                Expanded(
                  child: InkWell(
                    child: textDecoration(
                        text: 'Home',
                        icon: SvgPicture.asset(
                          'images/home_icon.svg',
                          color: addressValue == 'Home'
                              ? Colors.white
                              : Colors.black87,
                          width: 30.0,
                          height: 30.0,
                        ),
                        color: addressValue == 'Home'
                            ? primaryColor
                            : Colors.transparent,
                        textColor: addressValue == 'Home'
                            ? Colors.white
                            : Colors.black87),
                    onTap: () {
                      setState(() {
                        addressValue = 'Home';
                      });
                    },
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: InkWell(
                    child: textDecoration(
                        text: 'Work',
                        icon: SvgPicture.asset(
                          'images/office.svg',
                          color: addressValue == 'Work'
                              ? Colors.white
                              : Colors.black87,
                          width: 30.0,
                          height: 30.0,
                        ),
                        color: addressValue == 'Work'
                            ? primaryColor
                            : Colors.transparent,
                        textColor: addressValue == 'Work'
                            ? Colors.white
                            : Colors.black87),
                    onTap: () {
                      setState(() {
                        addressValue = 'Work';
                      });
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            ButtonTheme(
              padding: const EdgeInsets.all(15),
              minWidth: 200,
              child: Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                elevation: 10.0,
                clipBehavior: Clip.antiAlias,
                child: MaterialButton(
                  onPressed: () {
                    uploadAddress(context);
                  },
                  child: const Text("Save Address"),
                  color: primaryColor,
                  textColor: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void uploadAddress(BuildContext context) async {
    try {
      var dio = Dio();

      if (kDebugMode) {
        print(userAddressUrl);
      }
      await LoaderUtils().showAlertDialog(context);
      Response response;
      if (addressId != 0) {
        if (kDebugMode) {
          print("patch : " + addressId.toString());
        }
        response = await dio.patch(userAddressUrl, data: {
          'id': addressId,
          'fullName': fullNameController.text,
          'phoneNo': phoneNoController.text,
          'pinCode': pincodeController.text,
          'houseName': houseController.text,
          'landmark': landMarkController.text,
          'houseType': addressValue.toUpperCase(),
        });
      } else {
        response = await dio.post(userAddressUrl, data: {
          'userId': userId,
          'fullName': fullNameController.text,
          'phoneNo': phoneNoController.text,
          'pinCode': pincodeController.text,
          'houseName': houseController.text,
          'landmark': landMarkController.text,
          'houseType': addressValue.toUpperCase(),
        });
      }
      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          if (addressId != 0) {
            message("Address added successfully.", context);
          } else {
            message("Address updated successfully.", context);
          }
          Navigator.pop(context, response.data);
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }
}
