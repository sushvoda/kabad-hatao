import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kabadhatao/ColorFile.dart';
import 'package:kabadhatao/LoginScreen.dart';

import './ColorFile.dart';
import 'AnimatedToggle.dart';
import 'Common.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeWidgetState createState() => _WelcomeWidgetState();
}

class _WelcomeWidgetState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: <Widget>[
              const Logo(),
              Container(
                margin: const EdgeInsets.only(top: 40, left: 20),
                child: Column(
                  children: const [
                    Text(
                      "Kabad Hatao",
                      style: TextStyle(
                          fontSize: 40,
                          fontFamily: 'GOTHAMMEDIUM',
                          color: whiteColor),
                    ),
                    Text(
                      "Your Home & Office Clean Service Expert",
                      style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'GOTHAM',
                          color: whiteColor),
                    ),
                  ],
                ),
              )
            ],
          ),
          AnimatedToggle(
            values: const ['Sign In', 'Continue'],
            onToggleCallback: (value) {
              setState(() {
                var _duration = const Duration(seconds: 1);
                Timer(_duration, navigationPage);
              });
            },
            buttonColor: whiteColor,
            backgroundColor: primaryColor,
            textColor: textColorLite,
          ),
          Container(
            width: MediaQuery.of(context).size.width - 10,
            alignment: Alignment.bottomRight,
            child: Align(
              alignment: Alignment.bottomRight,
              child: MaterialButton(
                onPressed: () => {dashboardPage(context)},
                child: const Text(
                  "Skip",
                  style: TextStyle(
                      fontSize: 17, fontFamily: 'GOTHAM', color: primaryColor),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  navigationPage() async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => const LoginScreen()));
  }
}

class Logo extends StatelessWidget {
  const Logo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = const AssetImage('images/man.jpg');
    Image image = Image(image: assetImage);
    return SizedBox(child: image);
  }
}
