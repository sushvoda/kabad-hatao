import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kabadhatao/ColorFile.dart';
import 'package:kabadhatao/Drawer.dart';
import 'package:kabadhatao/Images.dart';
import 'package:kabadhatao/utils/NetworkClass.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/loader_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AddressesPage.dart';
import 'Common.dart';
import 'HomePage.dart';
import 'model/models.dart';

class PickupPage extends StatefulWidget {
  const PickupPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PickupPageWidgetState();
}

class _PickupPageWidgetState extends State<PickupPage> {
  String userName = "";
  String userMobile = "";
  String profilePic = "";
  int userId = 0;

  bool donateValue = false;
  bool agreedValue = false;
  String addressValue = "";

  dynamic imageFile1;
  dynamic imageFile2;
  dynamic imageFile3;

  late AddressModel selectedAddress;

  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = const TimeOfDay(hour: 00, minute: 00);

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController fullNameController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController pincodeController = TextEditingController();
  TextEditingController houseController = TextEditingController();
  TextEditingController landMarkController = TextEditingController();
  TextEditingController timeSlotController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  FocusNode fullName = FocusNode();
  FocusNode phoneNo = FocusNode();
  FocusNode address = FocusNode();
  FocusNode pincode = FocusNode();
  FocusNode houseNo = FocusNode();
  FocusNode landMark = FocusNode();
  FocusNode timeSlot = FocusNode();
  FocusNode dateSlot = FocusNode();

  @override
  void initState() {
    getValues();
    super.initState();
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userName = sharedPreferences.getString("name") ?? "";
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    profilePic = sharedPreferences.getString("profilePic") ?? "";
    userId = sharedPreferences.getInt("id") ?? 0;

    fullNameController.text = userName;
    phoneNoController.text = userMobile;

    setState(() {});
  }

  @override
  void dispose() {
    fullName.dispose();
    phoneNo.dispose();
    address.dispose();
    pincode.dispose();
    houseNo.dispose();
    landMark.dispose();
    timeSlot.dispose();
    dateSlot.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: true,
      drawer: drawer(
        context: context,
        userName: userName,
        userMobile: userMobile,
        profilePic: profilePic,
        isPartner: false,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).viewPadding.top),
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                  icon: svgDrawer,
                  onPressed: () => _scaffoldKey.currentState?.openDrawer(),
                ),
                logoKH(logoKHImage),
                const Spacer(flex: 1),
                svgSearch,
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 30,
                  child: InkWell(
                    onTap: () {
                      myCart(context);
                    },
                    child: svgCart,
                  ),
                ),
              ],
            ),
            Flexible(
              child: ListView(
                padding: const EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 20.0),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                children: [
                  makeInputWithBoarder(
                      label: 'Full Name (Required)*',
                      controller: fullNameController,
                      focusNode: fullName,
                      obscureText: false,
                      enabled: true,
                      hintText: 'Your Name'),
                  makeInputNumberWithBoarder(
                      label: 'Phone No. (Required)*',
                      controller: phoneNoController,
                      focusNode: phoneNo,
                      hintText: 'Your Number',
                      maxLength: 10,
                      heightSizedBox: 0.0),
                  const SizedBox(height: 20.0),
                  GestureDetector(
                    onTap: () {
                      openAddressesScreen(context);
                    },
                    child: makeInputWithBoarder(
                        label: 'Address',
                        obscureText: false,
                        enabled: false,
                        controller: addressController,
                        focusNode: address,
                        hintText: 'Enter your address'),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: makeInputNumberWithBoarder(
                            label: 'Pincode',
                            controller: pincodeController,
                            focusNode: pincode,
                            hintText: '',
                            maxLength: 6,
                            heightSizedBox: 0.0),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Center(
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              color: primaryColor,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(5.0)),
                              border: Border.all(color: Colors.grey),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  svgLocation,
                                  const SizedBox(width: 5),
                                  const Text(
                                    'Use my location',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20.0),
                  makeInputWithBoarder(
                      label: 'House No.Building Name (Required)*',
                      controller: houseController,
                      focusNode: houseNo,
                      obscureText: false,
                      enabled: true,
                      hintText: ''),
                  makeInputWithBoarder(
                      label: 'Landmark (Required)*',
                      controller: landMarkController,
                      focusNode: landMark,
                      obscureText: false,
                      enabled: true,
                      hintText: ''),
                  Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          readOnly: true,
                          controller: timeSlotController,
                          decoration: InputDecoration(
                              labelText: 'Time Slot',
                              hintText: "",
                              suffixIcon: Container(
                                padding: const EdgeInsets.all(10.0),
                                constraints: const BoxConstraints(
                                  maxHeight: 20.0,
                                  maxWidth: 20.0,
                                ),
                                child: SvgPicture.asset(
                                  'images/clock.svg',
                                ),
                              ),
                              hintMaxLines: 1,
                              counter: const Offstage(),
                              border: const OutlineInputBorder(),
                              labelStyle: const TextStyle(color: grayColor1),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.auto),
                          onTap: () {
                            _selectTime(context);
                          },
                        ), /*InkWell(
                          child: textDecoration(
                            text: 'Time Slot',
                            icon: SvgPicture.asset(
                              'images/clock.svg',
                              width: 30.0,
                              height: 30.0,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              addressValue = 'Time';
                            });
                          },
                        ),*/
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: TextFormField(
                          readOnly: true,
                          controller: dateController,
                          decoration: InputDecoration(
                              labelText: 'Pickup Date',
                              hintText: "",
                              suffixIcon: Container(
                                padding: const EdgeInsets.all(10.0),
                                constraints: const BoxConstraints(
                                  maxHeight: 20.0,
                                  maxWidth: 20.0,
                                ),
                                child: SvgPicture.asset(
                                  'images/calendar_icon.svg',
                                ),
                              ),
                              hintMaxLines: 1,
                              counter: const Offstage(),
                              border: const OutlineInputBorder(),
                              labelStyle: const TextStyle(color: grayColor1),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.auto),
                          onTap: () {
                            _selectDate(context);
                          },
                        ), /*InkWell(
                          child: textDecoration(
                            text: 'Pickup Date',
                            icon: SvgPicture.asset(
                              'images/calendar_icon.svg',
                              width: 30.0,
                              height: 30.0,
                            ),
                          ),
                          onTap: () {
                            _selectDate(context);
                            setState(() {
                              addressValue = 'Date';
                            });
                          },
                        ),*/
                      ),
                    ],
                  ),
                  const SizedBox(height: 20.0),
                  makeText(label: 'Type of address'),
                  const SizedBox(height: 5.0),
                  Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          child: textDecoration(
                              text: 'Home',
                              icon: SvgPicture.asset(
                                'images/home_icon.svg',
                                color: addressValue == 'Home'
                                    ? Colors.white
                                    : Colors.black87,
                                width: 30.0,
                                height: 30.0,
                              ),
                              color: addressValue == 'Home'
                                  ? primaryColor
                                  : Colors.transparent,
                              textColor: addressValue == 'Home'
                                  ? Colors.white
                                  : Colors.black87),
                          onTap: () {
                            setState(() {
                              addressValue = 'Home';
                            });
                          },
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: InkWell(
                          child: textDecoration(
                              text: 'Work',
                              icon: SvgPicture.asset(
                                'images/office.svg',
                                color: addressValue == 'Work'
                                    ? Colors.white
                                    : Colors.black87,
                                width: 30.0,
                                height: 30.0,
                              ),
                              color: addressValue == 'Work'
                                  ? primaryColor
                                  : Colors.transparent,
                              textColor: addressValue == 'Work'
                                  ? Colors.white
                                  : Colors.black87),
                          onTap: () {
                            setState(() {
                              addressValue = 'Work';
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20.0),
                  makeText(label: 'Capture & upload product image'),
                  const SizedBox(height: 5.0),
                  Row(
                    children: [
                      imageFile1 == null
                          ? InkWell(
                              child: Container(
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0)),
                                  border: Border.all(color: Colors.grey),
                                ),
                                child: const Center(
                                  child: Icon(Icons.add),
                                ),
                              ),
                              onTap: () {
                                _moreOptionsBottomSheetMenu(context, "image1");
                              },
                            )
                          : Stack(
                              alignment: Alignment.topRight,
                              children: [
                                Image.file(
                                  imageFile1,
                                  fit: BoxFit.fitWidth,
                                  height: 100,
                                  width: 100,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    imageFile1 = null;
                                    setState(() {});
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Icon(
                                      Icons.close,
                                      size: 20,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                      const SizedBox(width: 10.0),
                      imageFile2 == null
                          ? InkWell(
                              child: Container(
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0)),
                                  border: Border.all(color: Colors.grey),
                                ),
                                child: const Center(
                                  child: Icon(Icons.add),
                                ),
                              ),
                              onTap: () {
                                _moreOptionsBottomSheetMenu(context, "image2");
                              },
                            )
                          : Stack(
                              alignment: Alignment.topRight,
                              children: [
                                Image.file(
                                  imageFile2,
                                  fit: BoxFit.fitWidth,
                                  height: 100,
                                  width: 100,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    imageFile2 = null;
                                    setState(() {});
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Icon(
                                      Icons.close,
                                      size: 20,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                      const SizedBox(width: 10.0),
                      imageFile3 == null
                          ? InkWell(
                              child: Container(
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0)),
                                  border: Border.all(color: Colors.grey),
                                ),
                                child: const Center(
                                  child: Icon(Icons.add),
                                ),
                              ),
                              onTap: () {
                                _moreOptionsBottomSheetMenu(context, "image3");
                              },
                            )
                          : Stack(
                              alignment: Alignment.topRight,
                              children: [
                                Image.file(
                                  imageFile3,
                                  fit: BoxFit.fitWidth,
                                  height: 100,
                                  width: 100,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    imageFile3 = null;
                                    setState(() {});
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Icon(
                                      Icons.close,
                                      size: 20,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                    ],
                  ),
                  const SizedBox(height: 10.0),
                  CheckboxListTile(
                    title:
                        const Text("If you want to donate click on checkbox"),
                    value: donateValue,
                    activeColor: primaryColor,
                    onChanged: (newValue) {
                      setState(() {
                        donateValue = newValue!;
                      });
                    },
                    contentPadding: const EdgeInsets.all(0),
                    dense: false,
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                  Center(
                    child: Wrap(
                      children: <Widget>[
                        ButtonTheme(
                          padding: const EdgeInsets.all(15),
                          minWidth: 200,
                          child: Material(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            elevation: 10.0,
                            clipBehavior: Clip.antiAlias,
                            child: MaterialButton(
                              onPressed: () {},
                              child: const Text("Donate"),
                              color: donateValue ? primaryColor : textColorLite,
                              textColor: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  CheckboxListTile(
                    activeColor: primaryColor,
                    contentPadding: const EdgeInsets.all(0),
                    title: const Text(
                        "I agreed the user agreement & terms and conditions"),
                    value: agreedValue,
                    onChanged: (newValue) {
                      setState(() {
                        agreedValue = newValue!;
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                  ButtonTheme(
                    padding: const EdgeInsets.all(15),
                    minWidth: 200,
                    child: Material(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      elevation: 10.0,
                      clipBehavior: Clip.antiAlias,
                      child: MaterialButton(
                        onPressed: () {
                          uploadDocument(
                              context, imageFile1, imageFile2, imageFile3);
                        },
                        child: const Text("Save Address"),
                        color: agreedValue ? primaryColor : textColorLite,
                        textColor: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            /*Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child:  Stack(
                children: [
                  Column(
                    children: [
                      makeTextField(
                          label: '',
                          hintText: 'Search',
                          icons: const Icon(Icons.search)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: makeMaterialButton(
                                  label: "Edit",
                                  color: textColor,
                                  radiusValue: 0.0,
                                  context: context,
                                  onPage: const PickupRequestPage(),
                                ),
                              ),
                              const SizedBox(width: 2),
                              Expanded(
                                child: makeMaterialButton(
                                  label: "Confirm",
                                  radiusValue: 0.0,
                                  context: context,
                                  onPage: const PickupRequestPage(),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),*/
          ],
        ),
      ),
    );
  }

  _moreOptionsBottomSheetMenu(context, type) {
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return Container(
          color: Colors.transparent,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: ListBody(
              children: <Widget>[
                makeText(
                    label: 'Choose an action',
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
                const SizedBox(height: 20),
                GestureDetector(
                  child: const Text('Take a picture'),
                  onTap: () {
                    Navigator.pop(context);
                    _getFromCamera(type);
                  },
                ),
                const SizedBox(height: 20),
                GestureDetector(
                  child: const Text('Select from gallery'),
                  onTap: () {
                    Navigator.pop(context);
                    _getFromGallery(type);
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  /// Get from gallery
  _getFromGallery(type) async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      imageQuality: 25,
      maxWidth: MediaQuery.of(context).size.width,
      maxHeight: 400,
    );
    if (pickedFile != null) {
      setState(() {
        if (type == "image1") {
          imageFile1 = File(pickedFile.path);
        } else if (type == "image2") {
          imageFile2 = File(pickedFile.path);
        } else {
          imageFile3 = File(pickedFile.path);
        }
      });
    }
  }

  /// Get from Camera
  _getFromCamera(type) async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      imageQuality: 25,
      maxWidth: MediaQuery.of(context).size.width,
      maxHeight: 400,
    );
    if (pickedFile != null) {
      setState(() {
        if (type == "image1") {
          imageFile1 = File(pickedFile.path);
        } else if (type == "image2") {
          imageFile2 = File(pickedFile.path);
        } else {
          imageFile3 = File(pickedFile.path);
        }
      });
    }
  }

  void uploadFile(BuildContext context, File _image) async {
    try {
      bool a = selectedTime.period == DayPeriod.am;
      TimeOfDay selectedTime1 =
          selectedTime.replacing(hour: selectedTime.hourOfPeriod);
      var time = a
          ? "${selectedTime1.hour}:${selectedTime1.minute}AM"
          : "${selectedTime1.hour}:${selectedTime1.minute}PM";
      var formData = FormData.fromMap({
        'userId': userId,
        'fullName': fullNameController.text,
        'phoneNo': phoneNoController.text,
        'pinCode': pincodeController.text,
        'houseName': houseController.text,
        'landmark': landMarkController.text,
        'houseType': addressValue.toUpperCase(),
        'timeSlot': time,
        'pickupDate': selectedDate.millisecondsSinceEpoch,
        'image': await MultipartFile.fromFile(_image.path, filename: "")
      });

      var dio = Dio();

      if (kDebugMode) {
        print(userAddressPickupUrl);
        print(_image.path);
        print(formData.fields);
      }
      await LoaderUtils().showAlertDialog(context);

      var response = await dio.post(
        userAddressPickupUrl,
        data: formData,
        onSendProgress: (int sent, int total) {
          if (kDebugMode) {
            print('$sent $total');
          }
        },
      );
      if (kDebugMode) {
        print(response.data);
      }
      Navigator.pop(context);
      if (response.statusCode == 200) {
        if (response.data['success'] == false) {
          message(response.data['message'], context);
        } else if (response.data['success'] == true) {
          openPage(context, const HomePage());
        } else {
          message(response.data['message'], context);
        }
      } else {
        message(response.data['message'], context);
      }
    } on DioError catch (e) {
      if (kDebugMode) {
        print(e.error);
      }
      Navigator.pop(context);
      setState(() {});
      if (e.type == DioErrorType.response) {
        message('${e.error}', context);
        return;
      }
      if (e.type == DioErrorType.connectTimeout) {
        message('Please check your internet connection', context);
        return;
      }

      if (e.type == DioErrorType.receiveTimeout) {
        message('Unable to connect to the server', context);
        return;
      }

      if (e.type == DioErrorType.other) {
        message('Something went wrong', context);
        return;
      }
      message(e, context);
    } catch (e) {
      message(e.toString(), context);
      setState(() {});
    }
  }

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(selectedDate.year),
      lastDate: DateTime(selectedDate.year + 1),
      helpText: 'Select Pickup Date',
    );
    if (selected != null && selected != selectedDate) {
      setState(() {
        selectedDate = selected;
        dateController.text =
            "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}";
      });
    }
  }

  _selectTime(BuildContext context) async {
    final TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (newTime != null) {
      setState(() {
        selectedTime = newTime;
        timeSlotController.text = "${selectedTime.hour}:${selectedTime.minute}";
      });
    }
  }

  void openAddressesScreen(BuildContext context) async {
    var address = await Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => const AddressesPage()));
    if (address == null) return;
    selectedAddress = address;
    if (kDebugMode) {
      print(selectedAddress);
    }
    pincodeController.text = "${selectedAddress.pinCode}";
    houseController.text = selectedAddress.houseName!;
    landMarkController.text = selectedAddress.landmark!;
    if (selectedAddress.fullName!.isNotEmpty) {
      fullNameController.text = selectedAddress.fullName!;
    }
    if (selectedAddress.phoneNo!.isNotEmpty) {
      phoneNoController.text = selectedAddress.phoneNo!;
    }
    if (selectedAddress.houseType!.isNotEmpty) {
      addressValue =
          selectedAddress.houseType!.toUpperCase() == "HOME" ? "Home" : "Work";
    }

    setState(() {});
  }

  Future<void> uploadDocument(
      BuildContext context, File file1, File file2, File file3) async {
    await LoaderUtils().showAlertDialog(context);
    setState(() {});
    if (kDebugMode) {
      print(getUserProfileUrl);
      print(userId);
    }
    bool a = selectedTime.period == DayPeriod.am;
    TimeOfDay selectedTime1 =
        selectedTime.replacing(hour: selectedTime.hourOfPeriod);
    var time = a
        ? "${selectedTime1.hour}:${selectedTime1.minute}AM"
        : "${selectedTime1.hour}:${selectedTime1.minute}PM";

    NetworkClass()
        .dio_post_form_data_pickup(
            context,
            userAddressPickupUrl,
            userId,
            fullNameController.text,
            phoneNoController.text,
            pincodeController.text,
            houseController.text,
            landMarkController.text,
            addressValue.toUpperCase(),
            time,
            selectedDate.millisecondsSinceEpoch,
            file1,
            file2,
            file3)
        .then((value) {
      Navigator.pop(context);
      if (kDebugMode) {
        print("document upload response : comes");
      }
      if (value != null) {
        Response response = value;
        if (response.statusCode == 200) {
          if (kDebugMode) {
            print("response : " + response.data.toString());
          }
          Object intervention = response.data;
          if (intervention is String) {
            if (kDebugMode) {
              print(response.data.toString().toLowerCase());
            }
          } else {
            if (kDebugMode) {
              print("status message array : " + response.data.toString());
            }
          }
        } else {
          if (kDebugMode) {
            print("status code : " + response.statusCode.toString());
          }
        }
      } else {
        if (kDebugMode) {
          print("response null");
        }
      }
      setState(() {});
    });
  }
}
