
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/HomePage.dart';
import 'package:kabadhatao/Images.dart';
import 'package:kabadhatao/PriceListPage.dart';

import 'ColorFile.dart';
import 'Common.dart';
import 'MoreOptionsPage.dart';
import 'PickupPage.dart';
import 'ProfileMain.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageWidgetState();
}

class _DashboardPageWidgetState extends State<DashboardPage> {
  String userName = "";
  String userMobile = "";

  int _selectedIndex = 0;
  final PageController _pageController = PageController();

  @override
  void initState() {
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    _pageController.jumpToPage(index);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            bottomNavigationBarItem('images/home.svg',
                _selectedIndex == 0 ? primaryColor : grayColor),
            bottomNavigationBarItem('images/pickup.svg',
                _selectedIndex == 1 ? primaryColor : grayColor),
            bottomNavigationBarItem('images/options.svg',
                _selectedIndex == 2 ? primaryColor : grayColor),
            bottomNavigationBarItem('images/price_list.svg',
                _selectedIndex == 3 ? primaryColor : grayColor),
            bottomNavigationBarItem('images/profile.svg',
                _selectedIndex == 4 ? primaryColor : grayColor),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: primaryColor,
          unselectedItemColor: Colors.black54,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          onTap: _onItemTapped,
        ),
        body: PageView(
          controller: _pageController,
          children: const <Widget>[
            HomePage(),
            PickupPage(),
            MoreOptionsPage(),
            PriceListPage(),
            ProfileMain(),
          ],
          onPageChanged: (page) {
            setState(() {
              _selectedIndex = page;
            });
          },
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Are you sure?'),
            content: const Text('Do you want to exit an App?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text('No'),
              ),
              TextButton(
                onPressed: () => {
                  Future.delayed(const Duration(milliseconds: 100), () {
                    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  }),
                },
                child: const Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }

  _moreOptionsBottomSheetMenu(context) {
    showModalBottomSheet(
      context: context,
      useRootNavigator: true,
      builder: (builder) {
        return Container(
          height: 350.0,
          color: Colors.transparent,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                applicationColumn(
                  label: 'Whatsapp',
                  image: 'images/whatsapp.svg',
                ),
                applicationColumn(
                  label: 'Support',
                  image: 'images/support.svg',
                ),
                applicationColumn(
                  label: 'Bulk Enquiry',
                  image: 'images/bulk_enquiry.svg',
                ),
                applicationColumn(
                  label: 'Donate',
                  image: 'images/donate.svg',
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
