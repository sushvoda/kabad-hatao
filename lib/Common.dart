import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kabadhatao/AddressPage.dart';
import 'package:kabadhatao/AddressesPage.dart';
import 'package:kabadhatao/MyCartPage.dart';
import 'package:kabadhatao/WelcomeScreen.dart';
import 'package:kabadhatao/model/models.dart';

import 'ColorFile.dart';
import 'Dashboard.dart';
import 'LoginScreen.dart';

void hideKeyword(BuildContext context) {
  FocusScopeNode currentFocus = FocusScope.of(context);
  if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
    currentFocus.focusedChild?.unfocus();
  }
}

BoxDecoration boxDecoration() {
  return BoxDecoration(
    borderRadius: BorderRadius.circular(40),
    border: const Border(
      bottom: BorderSide(color: Colors.black),
      top: BorderSide(color: Colors.black),
      right: BorderSide(color: Colors.black),
      left: BorderSide(color: Colors.black),
    ),
  );
}

InputDecoration inputDecoration() {
  return const InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.grey),
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.grey),
    ),
    counter: Offstage(),
  );
}

InputDecoration inputDecorationForNumber() {
  return const InputDecoration(
    counter: Offstage(),
    isDense: true,
    hintText: "0",
    border: InputBorder.none,
    contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
  );
}

Widget makeText({
  label,
  fontWeight,
  color = Colors.black87,
  textAlign = TextAlign.start,
  fontSize = 15.0,
  alignment = Alignment.centerLeft,
}) {
  return Flexible(
    child: Container(
      alignment: alignment,
      child: Text(
        label,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style:
            TextStyle(fontSize: fontSize, fontWeight: fontWeight, color: color),
        textAlign: textAlign,
      ),
    ),
  );
}

Widget makeInputForNumber({label, controller, focusNode}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      makeText(label: label),
      const SizedBox(height: 5),
      TextField(
        maxLength: 3,
        controller: controller,
        focusNode: focusNode,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
        keyboardType: TextInputType.number,
        // Only numbers can be entered
        decoration: const InputDecoration(
          counter: Offstage(),
          isDense: true,
          focusColor: primaryColor,
          hintText: "0",
          contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
      ),
      const SizedBox(height: 30.0)
    ],
  );
}

Widget makeInputForMobile({controller, focusNode, belowPadding = 30.0}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      makeText(label: 'Phone Number', alignment: Alignment.centerLeft),
      const SizedBox(height: 5.0),
      TextField(
        focusNode: focusNode,
        maxLength: 10,
        controller: controller,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
        keyboardType: TextInputType.number,
        // Only numbers can be entered
        decoration: const InputDecoration(
          counter: Offstage(),
          prefixText: '+91',
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
      ),
      SizedBox(height: belowPadding)
    ],
  );
}

Widget makeInput(
    {required String label,
    required String hintText,
    obscureText,
    controller,
    focusNode,
    enabled}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      TextFormField(
        enabled: enabled,
        focusNode: focusNode,
        obscureText: obscureText,
        controller: controller,
        decoration: InputDecoration(
          hintText: hintText,
          labelText: label,
          labelStyle: const TextStyle(color: Color(0xFF424242)),
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
      ),
      const SizedBox(height: 30)
    ],
  );
}

Widget makeMaterialButton(
    {required String label,
    enabled,
    color = primaryColor,
    radiusValue = 5.0,
    context,
    onPage}) {
  return Material(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radiusValue)),
    elevation: 10.0,
    color: color,
    clipBehavior: Clip.antiAlias,
    child: MaterialButton(
      padding: const EdgeInsets.all(15),
      child: Text(label,
          style: const TextStyle(fontSize: 16.0, color: Colors.white)),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) => onPage));
      },
    ),
  );
}

Column applicationColumn({label, image = ""}) {
  return Column(
    children: [
      CircleAvatar(
        backgroundColor: lightGreen,
        radius: 30,
        child: SvgPicture.asset(
          image,
          color: primaryColor,
          width: 30.0,
          height: 30.0,
        ), //Text
      ),
      const SizedBox(height: 5.0),
      makeText(label: label, fontSize: 10.0),
    ],
  );
}

Column applicationColumnNetwork({label, image = ""}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      CircleAvatar(
        backgroundColor: lightGreen,
        radius: 35,
        child: FadeInImage.assetNetwork(
          placeholder: "images/loading.gif",
          image: image,
          width: 50.0,
          height: 50.0,
          imageErrorBuilder: (context, error, stackTrace) {
            return const SizedBox(
              child: Image(
                image: AssetImage('images/placeholder.png'),
                width: 50,
                height: 50,
              ),
            );
          },
        ),
      ),
      const SizedBox(height: 5.0),
      makeText(
          label: label,
          fontSize: 10.0,
          textAlign: TextAlign.center,
          alignment: Alignment.center),
    ],
  );
}

Widget logoKH(selectImg) {
  return Image(image: selectImg, height: 60, width: 100);
}

Widget makeTextField(
    {label = "",
    hintText = "",
    controller,
    focusNode,
    belowPadding = 10.0,
    icons,
    suffixIcons}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      makeText(label: label, alignment: Alignment.centerLeft),
      const SizedBox(height: 5.0),
      TextField(
        focusNode: focusNode,
        maxLength: 100,
        controller: controller,
        // Only numbers can be entered
        decoration: InputDecoration(
            alignLabelWithHint: true,
            prefixIcon: icons,
            suffixIcon: suffixIcons,
            counter: const Offstage(),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
            ),
            border: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
            ),
            hintText: hintText),
      ),
      SizedBox(height: belowPadding)
    ],
  );
}

Widget makeInputWithBoarder(
    {required String label,
    required String hintText,
    obscureText = false,
    controller,
    focusNode,
    enabled = true,
    heightSizedBox = 20.0}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      TextFormField(
        enabled: enabled,
        focusNode: focusNode,
        obscureText: obscureText,
        controller: controller,
        decoration: InputDecoration(
          hintText: hintText,
          labelText: label,
          counter: const Offstage(),
          border: const OutlineInputBorder(),
          labelStyle: const TextStyle(color: grayColor1),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
      ),
      SizedBox(height: heightSizedBox)
    ],
  );
}

Widget textDecoration({text, icon, color, textColor}) {
  return DecoratedBox(
    decoration: BoxDecoration(
      color: color,
      borderRadius: const BorderRadius.all(Radius.circular(5.0)),
      border: Border.all(color: Colors.grey),
    ),
    child: Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
      child: Row(
        children: [
          Text(
            text,
            style: TextStyle(color: textColor),
          ),
          const Spacer(),
          icon,
        ],
      ),
    ),
  );
}

Widget makeInputNumberWithBoarder(
    {required String label,
    required String hintText,
    obscureText = false,
    controller,
    focusNode,
    enabled = true,
    maxLength,
    heightSizedBox = 20.0}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      TextFormField(
        enabled: enabled,
        focusNode: focusNode,
        obscureText: obscureText,
        maxLength: maxLength,
        controller: controller,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: hintText,
          labelText: label,
          counter: const Offstage(),
          border: const OutlineInputBorder(),
          labelStyle: const TextStyle(color: grayColor1),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
        ),
      ),
      SizedBox(height: heightSizedBox)
    ],
  );
}

Widget makeMaterialButtonWithIcon(
    {required String label,
    enabled,
    color = primaryColor,
    radiusValue = 5.0,
    context,
    onPage,
    icons}) {
  return Material(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radiusValue)),
    elevation: 10.0,
    color: color,
    clipBehavior: Clip.antiAlias,
    child: MaterialButton(
      minWidth: 350.0,
      height: 40,
      child: Row(
        children: [
          icons,
          const SizedBox(width: 1),
          Text(label,
              style: const TextStyle(fontSize: 14.0, color: Colors.white))
        ],
      ),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) => onPage));
      },
    ),
  );
}

void myCart(BuildContext context) {
  Navigator.of(context).push(
      MaterialPageRoute(builder: (BuildContext context) => const MyCartPage()));
}

message(message, context) {
  final snackBar = SnackBar(content: Text(message));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

dashboardPage(BuildContext context) {
  Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (BuildContext context) => const DashboardPage()));
}

welcomePage(BuildContext context) {
  Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (BuildContext context) => const WelcomeScreen()));
}

void loginScreen(BuildContext context) {
  Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (BuildContext context) => const LoginScreen()));
}

void addressScreen(BuildContext context) {
  Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => AddressPage(AddressModel())));
}

void addressesScreen(BuildContext context) {
  Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => const AddressesPage()));
}
