import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kabadhatao/Common.dart';

class DownloadInvoicePage extends StatefulWidget {
  const DownloadInvoicePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DownloadInvoicePageWidgetState();
}

class _DownloadInvoicePageWidgetState extends State<DownloadInvoicePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Download Invoice'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Container(),
    );
  }
}
