import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kabadhatao/ColorFile.dart';

AssetImage logoImage = const AssetImage('images/logo.png');
AssetImage logoKHImage = const AssetImage('images/logo_kh.png');
AssetImage splashBg = const AssetImage('images/splash_bg.jpg');
AssetImage splashLogo = const AssetImage('images/splash_kh_logo.png');
AssetImage drawerBg = const AssetImage('images/slider_topbg.jpg');

final Widget svgDrawer = SvgPicture.asset(
  'images/drawer.svg',
  color: primaryColor,
  width: 20.0,
  height: 20.0,
);

final Widget svgSearch = SvgPicture.asset(
  'images/search.svg',
  color: primaryColor,
  width: 30.0,
  height: 30.0,
);

final Widget svgUserIcon = SvgPicture.asset(
  'images/user_icon.svg',
  color: Colors.white,
  height: 50,
  width: 50,
);

final Widget svgCart = SvgPicture.asset(
  'images/cart.svg',
  color: primaryColor,
  width: 30.0,
  height: 30.0,
);

final Widget svgLocation = SvgPicture.asset(
  'images/location_icon.svg',
  color: Colors.white,
  width: 30.0,
  height: 30.0,
);

BottomNavigationBarItem bottomNavigationBarItem(image, color) {
  return BottomNavigationBarItem(
    icon: SvgPicture.asset(image, color: color, width: 30.0, height: 30.0),
    label: "",
  );
}
