import 'package:flutter/material.dart';
import 'package:kabadhatao/Drawer.dart';
import 'package:kabadhatao/Images.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ColorFile.dart';
import 'Common.dart';
import 'StringFile.dart';

class PriceListPage extends StatefulWidget {
  const PriceListPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PriceListPageWidgetState();
}

class _PriceListPageWidgetState extends State<PriceListPage> {
  String userName = "";
  String userMobile = "";
  String profilePic = "";

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    getValues();
    super.initState();
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userName = sharedPreferences.getString("name") ?? kabadHatao;
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    profilePic = sharedPreferences.getString("profilePic") ?? "";

    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer(
        context: context,
        userName: userName,
        userMobile: userMobile,
        profilePic: profilePic,
        isPartner: false,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).viewPadding.top),
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                  icon: svgDrawer,
                  onPressed: () => _scaffoldKey.currentState?.openDrawer(),
                ),
                logoKH(logoKHImage),
                const Spacer(flex: 1),
                svgSearch,
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 30,
                  child: InkWell(
                    onTap: () {
                      myCart(context);
                    },
                    child: svgCart,
                  ),
                ),
              ],
            ),
            Flexible(
              child: SizedBox(
                width: double.infinity,
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: 10,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        const SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Stack(
                            children: const [
                              Price1Image(),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 10.0, right: 10.0, top: 200),
                                child: DecoratedBox(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      color: primaryColor),
                                  child: Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Text(
                                      "Book for Pickup",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Stack(
                            children: const [
                              Price2Image(),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 10.0, right: 10.0, top: 200),
                                child: DecoratedBox(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      color: primaryColor),
                                  child: Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Text(
                                      "Book for Pickup",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Price1Image extends StatelessWidget {
  const Price1Image({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = const AssetImage('images/price1.png');
    Image image = Image(image: assetImage);
    return SizedBox(child: image);
  }
}

class Price2Image extends StatelessWidget {
  const Price2Image({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = const AssetImage('images/price2.png');
    Image image = Image(image: assetImage);
    return SizedBox(child: image);
  }
}
