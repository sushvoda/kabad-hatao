import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kabadhatao/Common.dart';
import 'package:kabadhatao/PickupListPage.dart';
import 'package:kabadhatao/ProfilePage.dart';
import 'package:kabadhatao/utils/URLConstants.dart';
import 'package:kabadhatao/utils/preference.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ColorFile.dart';
import 'Drawer.dart';
import 'Images.dart';
import 'PersonalInformationPage.dart';
import 'StringFile.dart';

class ProfileMain extends StatefulWidget {
  const ProfileMain({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfileMainWidgetState();
}

final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

class _ProfileMainWidgetState extends State<ProfileMain> {
  int userId = 0;
  String userName = "";
  String userMobile = "";
  String userEmail = "";
  String profilePic = "";
  bool isPartner = false;
  bool isLoggedIn = false;

  @override
  void initState() {
    getValues();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getInt("id") ?? 0;
    userName = sharedPreferences.getString("name") ?? kabadHatao;
    userMobile = sharedPreferences.getString("phoneNo") ?? "";
    userEmail = sharedPreferences.getString("email") ?? "";
    profilePic = sharedPreferences.getString("profilePic") ?? "";
    isLoggedIn = sharedPreferences.getBool("is_logged_in") ?? false;

    if (kDebugMode) {
      print("userId : " + userId.toString());
      print("userName : " + userName);
      print("userMobile : " + userMobile);
      print("profilePic : " + url + profilePic);
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grayBG,
      resizeToAvoidBottomInset: true,
      key: _scaffoldKey,
      drawer: drawer(
        context: context,
        userName: userName,
        userMobile: userMobile,
        profilePic: profilePic,
        isPartner: false,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).viewPadding.top),
        child: Column(children: [
          ColoredBox(
            color: Colors.white,
            child: Row(
              children: [
                IconButton(
                  icon: svgDrawer,
                  onPressed: () => _scaffoldKey.currentState?.openDrawer(),
                ),
                logoKH(logoKHImage),
                const Spacer(flex: 1),
              ],
            ),
          ),
          Flexible(
            child: ListView(
              padding: const EdgeInsets.all(2.0),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: [
                SizedBox(
                  height: 120,
                  child: Center(
                    child: Card(
                      color: primaryColor,
                      child: Row(
                        children: <Widget>[
                          const SizedBox(width: 10),
                          profilePic.isEmpty
                              ? SizedBox(
                                  width: 50,
                                  height: 50,
                                  child: CircleAvatar(
                                    backgroundColor: grayLiteBG,
                                    child: SvgPicture.asset(
                                      'images/user_icon.svg',
                                      color: Colors.white,
                                      width: 30.0,
                                      height: 30.0,
                                    ),
                                  ),
                                )
                              : ClipOval(
                                  child: Image.network(
                                    url + profilePic,
                                    fit: BoxFit.cover,
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              makeText(
                                label: userName,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0,
                              ),
                              makeText(
                                label: userMobile,
                                color: Colors.white,
                                fontSize: 12.0,
                              ),
                            ],
                          ),
                          const Spacer(),
                          Container(
                            padding: const EdgeInsets.all(35.0),
                            child: OutlinedButton(
                              onPressed: () {
                                Preference().clearPreference();
                                loginScreen(context);
                              },
                              style: OutlinedButton.styleFrom(
                                side: BorderSide(
                                  color: userName.isNotEmpty
                                      ? textColorLite
                                      : primaryColor,
                                ),
                              ),
                              child: makeText(
                                color: whiteColor,
                                label: isLoggedIn ? "Logout" : "Login",
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          const SizedBox(width: 10)
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  child: Center(
                    child: Card(
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(8.0),
                        leading: const Icon(Icons.info_outlined),
                        title: const Text(
                          'Orders',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        onTap: () {
                          if(isLoggedIn){
                            openPageNew(context, const PickupListPage());
                          }else{
                            message('Please login first.', context);
                          }

                        },
                        trailing: const Icon(Icons.chevron_right),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  child: Center(
                    child: Card(
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(8.0),
                        leading: const Icon(Icons.info_outlined),
                        title: const Text(
                          'Download Invoice',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        onTap: () {},
                        trailing: const Icon(Icons.chevron_right),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  child: Center(
                    child: Card(
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(8.0),
                        leading: const Icon(Icons.info_outlined),
                        title: const Text(
                          'Customer Support',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        onTap: () {},
                        trailing: const Icon(Icons.chevron_right),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  child: Center(
                    child: Card(
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(8.0),
                        leading: const Icon(Icons.info_outlined),
                        title: const Text(
                          'Profile',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        onTap: () {
                          if(isLoggedIn){
                            openPageNew(context, const ProfilePage());
                          }else{
                            message('Please login first.', context);
                          }

                        },
                        trailing: const Icon(Icons.chevron_right),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  child: Center(
                    child: Card(
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(8.0),
                        leading: const Icon(Icons.info_outlined),
                        title: const Text(
                          'Address',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        onTap: () {
                          if(isLoggedIn){
                            addressesScreen(context);
                          }else{
                            message('Please login first.', context);
                          }

                        },
                        trailing: const Icon(Icons.chevron_right),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  child: Center(
                    child: Card(
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(8.0),
                        leading: const Icon(Icons.info_outlined),
                        title: const Text(
                          'Notification',
                          style: TextStyle(fontSize: 16.0),
                        ),
                        onTap: () {},
                        trailing: const Icon(Icons.chevron_right),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
