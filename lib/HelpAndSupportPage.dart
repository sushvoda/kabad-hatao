import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

import 'ColorFile.dart';
import 'Common.dart';
import 'StringFile.dart';

class HelpAndSupportPage extends StatefulWidget {
  const HelpAndSupportPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HelpAndSupportPageWidgetState();
}

class _HelpAndSupportPageWidgetState extends State<HelpAndSupportPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: makeText(label: 'Help and Support'),
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_outlined,
            size: 20,
            color: Colors.black87,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
        actions: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: makeText(
                label: 'FAQ',
                alignment: Alignment.center,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          Card(
            margin: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 10.0, bottom: 5.0),
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  const SizedBox(height: 5.0),
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('images/whatsapp_icon.png'),
                  ),
                  const SizedBox(width: 20.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      makeText(
                        label: 'If need any help call now',
                        color: grayColor,
                      ),
                      const SizedBox(height: 5.0),
                      makeText(
                        label: '+91 9891746770',
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ],
                  ),
                  const Spacer(flex: 1),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    elevation: 10.0,
                    color: primaryColor,
                    clipBehavior: Clip.antiAlias,
                    child: MaterialButton(
                      child: makeText(label: 'Call', color: Colors.white),
                      onPressed: () {
                        _launchWhatsapp();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 10.0, bottom: 5.0),
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  const SizedBox(height: 5.0),
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('images/gmail_icon.png'),
                  ),
                  const SizedBox(width: 20.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      makeText(
                        label: 'Feel to free contact us',
                        color: grayColor,
                      ),
                      const SizedBox(height: 5.0),
                      makeText(
                        label: supportMail,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ],
                  ),
                  const Spacer(flex: 1),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    elevation: 10.0,
                    color: primaryColor,
                    clipBehavior: Clip.antiAlias,
                    child: MaterialButton(
                      child: makeText(label: 'Email', color: Colors.white),
                      onPressed: () {
                        launchEmailSubmission();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 10.0, bottom: 5.0),
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  const SizedBox(height: 5.0),
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('images/instagram_icon.png'),
                  ),
                  const SizedBox(width: 20.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      makeText(
                        label: 'Feel to free contact us',
                        color: grayColor,
                      ),
                      const SizedBox(height: 5.0),
                      makeText(
                        label: '@kabadhatao',
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ],
                  ),
                  const Spacer(flex: 1),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    elevation: 10.0,
                    color: primaryColor,
                    clipBehavior: Clip.antiAlias,
                    child: MaterialButton(
                      child: makeText(label: 'Follow', color: Colors.white),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 10.0, bottom: 5.0),
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  const SizedBox(height: 5.0),
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('images/fb_icon.png'),
                  ),
                  const SizedBox(width: 20.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      makeText(
                        label: 'Feel to free contact us',
                        color: grayColor,
                      ),
                      const SizedBox(height: 5.0),
                      makeText(
                        label: '@kabadhatao',
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ],
                  ),
                  const Spacer(flex: 1),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    elevation: 10.0,
                    color: primaryColor,
                    clipBehavior: Clip.antiAlias,
                    child: MaterialButton(
                      child: makeText(label: 'Like', color: Colors.white),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 10.0, bottom: 5.0),
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  const SizedBox(height: 5.0),
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('images/twitter_icon.png'),
                  ),
                  const SizedBox(width: 20.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      makeText(
                        label: 'Feel to free contact us',
                        color: grayColor,
                      ),
                      const SizedBox(height: 5.0),
                      makeText(
                        label: '@kabadhatao',
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ],
                  ),
                  const Spacer(flex: 1),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    elevation: 10.0,
                    color: primaryColor,
                    clipBehavior: Clip.antiAlias,
                    child: MaterialButton(
                      child: makeText(label: 'Follow', color: Colors.white),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 10.0, bottom: 5.0),
            elevation: 5.0,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  const SizedBox(height: 5.0),
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage('images/website_icon.png'),
                  ),
                  const SizedBox(width: 20.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      makeText(
                        label: 'More details on website',
                        color: grayColor,
                      ),
                      const SizedBox(height: 5.0),
                      makeText(
                        label: webUrl,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ],
                  ),
                  const Spacer(flex: 1),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    elevation: 10.0,
                    color: primaryColor,
                    clipBehavior: Clip.antiAlias,
                    child: MaterialButton(
                      child: makeText(label: 'Go', color: Colors.white),
                      onPressed: () {
                        _launchWebURL();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void launchEmailSubmission() async {
    final Uri params = Uri(
        scheme: 'mailto',
        path: supportMail,
        queryParameters: {'subject': mailSubject, 'body': mailBody});
    String url = params.toString();
    if (await canLaunch(url)) {
      await launch(
        getUrl(
          'mailto',
          supportMail,
          {'subject': mailSubject, 'body': mailBody},
        ),
      );
    } else {
      if (kDebugMode) {
        print('Could not launch $url');
      }
    }
  }

  String getUrl(
      String scheme, String path, Map<String, String> queryParameters) {
    String url = '$scheme:$path?';

    queryParameters.forEach((String k, String v) {
      url += '$k=$v&';
    });

    return url;
  }

  _launchWhatsapp() async {
    const url = "https://wa.me/$supportWhatsapp?text=$whatsappMessage";
    if (await canLaunch(url)) {
      launch(Uri.encodeFull(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchWebURL() async {
    const url = 'http://www.kabadhatao.com/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
