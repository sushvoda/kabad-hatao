import 'package:intl/intl.dart';

String convertDateFormat(
    String dateTimeString, String oldFormat, String newFormat) {
  DateFormat newDateFormat = DateFormat(newFormat);
  DateTime dateTime = DateFormat(oldFormat).parse(dateTimeString);
  String selectedDate = newDateFormat.format(dateTime);
  return selectedDate;
}
