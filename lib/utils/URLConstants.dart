const url = "http://ec2-13-235-249-204.ap-south-1.compute.amazonaws.com:8080";
const api = "/api/";
const signupUrl = url + api + "sign_in";
const verifyOtpUrl = url + api + "verify_otp";
const getUserProfileUrl = url + api + "user_profile";
const getUserPickupUrl = url + api + "user_pickup";
const userAddressPickupUrl = url + api + "user_address_pickup";
const userAddressUrl = url + api + "user_address";
const orderListUrl = url + api + "order_list/";
const cancelOrderUrl = url + api + "cancel_order/";
const productCategoryUrl = url + api + "product_category/0/page_index";

