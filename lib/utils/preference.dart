import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Preference {
  setName(String name) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("name", name);
    if (kDebugMode) {
      print(sharedPreferences.getString("name"));
    }
  }

  Future<String> getName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("name") ?? "";
  }

  setId(int id) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt("id", id);
    if (kDebugMode) {
      print(sharedPreferences.getInt("id"));
    }
  }

  setProfilePic(String profilePic) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("profilePic", profilePic);
    if (kDebugMode) {
      print(sharedPreferences.getString("profilePic"));
    }
  }

  setEmail(String email) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("email", email);
    if (kDebugMode) {
      print("email : ${sharedPreferences.getString("email")}");
    }
  }

  setPhoneNo(String phoneNo) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("phoneNo", phoneNo);
    if (kDebugMode) {
      print("phoneNo : ${sharedPreferences.getString("phoneNo")}");
    }
  }

  setLoggedIn(bool isLogin) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("is_logged_in", isLogin);
    if (kDebugMode) {
      print(sharedPreferences.getBool("is_logged_in"));
    }
  }

  void setUserDetails(data) {
    setName(data['name'] ?? "");
    setId(data['id'] ?? 0);
    setEmail(data['email'] ?? "");
    setProfilePic(data['profilePic'] ?? "");
    setPhoneNo(data['sms'] ['phoneNo'] ?? "");
    setLoggedIn(true);
  }

  void setUserProfile(data) {
    setName(data['name'] ?? "");
    setId(data['id'] ?? 0);
    setEmail(data['email'] ?? "");
    setProfilePic(data['profilePic'] ?? "");
    setLoggedIn(true);
  }

  void clearPreference() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
  }
}
