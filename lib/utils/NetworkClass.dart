import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:kabadhatao/utils/URLConstants.dart';

class NetworkClass {
  Dio dio = Dio();

  NetworkClass() {
    if (dio == null) {
      BaseOptions options = BaseOptions(
          baseUrl: url,
          receiveDataWhenStatusError: true,
          connectTimeout: 60 * 1000, // 60 seconds
          receiveTimeout: 60 * 1000 // 60 seconds
          );

      dio = Dio(options);
    }
  }

  Future<dynamic> dio_get(String url, BuildContext context) async {
    //SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      /*dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
        var customHeaders = {
          "Authorization": sharedPreferences.getString("token")
        };
        options.headers.addAll(customHeaders);
        return options;
      }));*/
      Response response = await dio.get(url);
      // if (response.statusCode == 200) {
      return response;
      // } else {
      //   toast(response.statusCode.toString());
      // }
    } on DioError catch (e) {
      return e.response;
      // show_network_exception(e);
    }
  }

  Future<Response?> dio_post(String url, BuildContext context, Map body) async {
    try {
      Response response = await dio.post(url, data: body);
      //if (response.statusCode == 200) {
      return response;
      // }
    } on DioError catch (e) {
      //show_network_exception(e);
      return e.response;
    }
  }

  Future<Response?> dio_post_form_data(
      BuildContext context, String url, File file, int userId) async {
    try {
      FormData formData = FormData.fromMap({
        "image": await MultipartFile.fromFile(file.path),
        'userId': userId,
        'email': "shivam@gmail.com",
        'name': "Shivam",
      });
      Response response = await dio.post(url, data: formData);

      return response;
      // }
    } on DioError catch (e) {
      // show_network_exception(e);
      return e.response;
    }
  }

  Future<Response?> dio_post_form_data_pickup(
      BuildContext context,
      String url,
      int userId,
      String fullName,
      String phoneNo,
      String pinCode,
      String houseName,
      String landmark,
      String houseType,
      String timeSlot,
      int pickupDate,
      File file1,
      File file2,
      File file3) async {
    try {
      FormData formData = FormData.fromMap({
        "image": await MultipartFile.fromFile(file1.path),
        "image": await MultipartFile.fromFile(file2.path),
        "image": await MultipartFile.fromFile(file3.path),
        'userId': userId,
        'phoneNo': phoneNo,
        'fullName': fullName,
        'pinCode': pinCode,
        'houseName': houseName,
        'landmark': landmark,
        'houseType': houseType,
        'timeSlot': timeSlot,
        'pickupDate': pickupDate,
      });
      Response response = await dio.post(url, data: formData);

      return response;
      // }
    } on DioError catch (e) {
      // show_network_exception(e);
      return e.response;
    }
  }

  Future<Response?> dio_login(
    String url,
    BuildContext context,
    var body,
  ) async {
    try {
      var dio = Dio();
      Response response =
          await dio.post(url, data: body, options: Options(sendTimeout: 5000));
      //if (response.statusCode == 200) {
      return response;
      // }
    } on DioError catch (e) {
      // show_network_exception(e);
      return e.response;
    }
  }
}
