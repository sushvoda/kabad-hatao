import 'package:flutter/material.dart';

import 'loader.dart';

class LoaderUtils {
  Future<void> showAlertDialog(BuildContext context) async {
    AlertDialog alert = AlertDialog(
      backgroundColor: Colors.white,
      content: SizedBox(height: 100.0, child: loader()),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
