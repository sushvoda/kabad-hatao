import 'package:flutter/cupertino.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

Widget loader() {
  return Center(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('images/loading.gif'),
        const SizedBox(height: 20.0),
        Text("Please Wait...".tr),
      ],
    ),
  );
}
