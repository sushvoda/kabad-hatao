class AddressModel {
  int? id = 0;
  int? pinCode = 0;
  String? houseName = "";
  String? landmark = "";
  String? houseType = "";
  PickupModel? pickUp;
  String? fullName = "";
  String? phoneNo = "";
  bool? active = false;

  AddressModel({
    this.id,
    this.pinCode,
    this.houseName,
    this.landmark,
    this.houseType,
    this.pickUp,
    this.fullName,
    this.phoneNo,
    this.active,
  });

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      id: json["id"] ?? 0,
      pinCode: json["pinCode"] ?? 0,
      houseName: json["houseName"] ?? "",
      landmark: json["landmark"] ?? "",
      houseType: json["houseType"] ?? "",
      pickUp: json.containsKey("containsKey") ? json["pickUp"] : null,
      fullName: json["fullName"] ?? "",
      phoneNo: json["phoneNo"] ?? "",
      active: json["active"] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['pinCode'] = pinCode;
    data['houseName'] = houseName;
    data['landmark'] = landmark;
    data['houseType'] = houseType;
    data['pickUp'] = pickUp;
    data['fullName'] = fullName;
    data['phoneNo'] = phoneNo;
    data['active'] = active;
    return data;
  }

  @override
  String toString() {
    return 'AddressModel{id: $id, pinCode: $pinCode, houseName: $houseName, landmark: $landmark, houseType: $houseType, fullName: $fullName, phoneNo: $phoneNo, active: $active, pickUp: $pickUp}';
  }
}

class PickupModel {
  int? id = 0;
  String? timeSlot = "";
  String? date = "";
  String? imageUrl = "";

  // int? addressId = 0;

  PickupModel({
    this.id,
    this.timeSlot,
    this.date,
    this.imageUrl,
    // this.addressId,
  });

  factory PickupModel.fromJson(Map<String, dynamic> json) {
    return PickupModel(
      id: json["id"] ?? 0,
      timeSlot: json["timeSlot"] ?? "",
      date: json["date"] ?? "",
      imageUrl: json["imageUrl"] ?? "",
      // addressId: json["addressId"] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['timeSlot'] = timeSlot;
    data['date'] = date;
    data['imageUrl'] = imageUrl;
    // data['addressId'] = addressId;
    return data;
  }

  @override
  String toString() {
    return 'PickupModel{id: $id, timeSlot: $timeSlot, date: $date, imageUrl: $imageUrl}';
  }
}

class OrderModel {
  int? id = 0;
  int? pinCode = 0;
  String? houseName = "";
  String? landmark = "";
  String? houseType = "";
  String? fullName = "";
  String? phoneNo = "";
  String? date = "";
  String? timeSlot = "";
  String? imageUrl = "";
  bool? cancelled = false;

  OrderModel({
    this.id,
    this.pinCode,
    this.houseName,
    this.landmark,
    this.houseType,
    this.fullName,
    this.phoneNo,
    this.date,
    this.timeSlot,
    this.imageUrl,
    this.cancelled,
  });

  factory OrderModel.fromJson(Map<String, dynamic> json) {
    return OrderModel(
      id: json["id"] ?? 0,
      pinCode: json["pinCode"] ?? 0,
      houseName: json["houseName"] ?? "",
      landmark: json["landmark"] ?? "",
      houseType: json["houseType"] ?? "",
      fullName: json["fullName"] ?? "",
      phoneNo: json["phoneNo"] ?? "",
      date: json["date"] ?? "",
      timeSlot: json["timeSlot"] ?? "",
      imageUrl: json["imageUrl"] ?? "",
      cancelled: json["cancelled"] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['pinCode'] = pinCode;
    data['houseName'] = houseName;
    data['landmark'] = landmark;
    data['houseType'] = houseType;
    data['fullName'] = fullName;
    data['phoneNo'] = phoneNo;
    data['date'] = date;
    data['timeSlot'] = timeSlot;
    data['imageUrl'] = imageUrl;
    data['cancelled'] = cancelled;
    return data;
  }

  @override
  String toString() {
    return 'OrderModel{id: $id, pinCode: $pinCode, houseName: $houseName, landmark: $landmark, houseType: $houseType, fullName: $fullName, phoneNo: $phoneNo, date: $date, timeSlot: $timeSlot, imageUrl: $imageUrl, cancelled: $cancelled}';
  }
}

class ProductCategoryModel {
  int? id = 0;
  String? imageUrl = "";
  String? text = "";
  List<ProductModel>? productsList;

  ProductCategoryModel({
    this.id,
    this.imageUrl,
    this.text,
    this.productsList,
  });

  factory ProductCategoryModel.fromJson(Map<String, dynamic> json) {
    var rest = json["products"];

    List<ProductModel> list =
        rest.map<ProductModel>((json) => ProductModel.fromJson(json)).toList();
    return ProductCategoryModel(
      id: json["id"] ?? 0,
      imageUrl: json["imageUrl"] ?? "",
      text: json["text"] ?? "",
      productsList: list,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['imageUrl'] = imageUrl;
    data['text'] = text;
    data['products'] = productsList;
    return data;
  }

  @override
  String toString() {
    return 'ProductCategoryModel{id: $id, imageUrl: $imageUrl, text: $text, products: $productsList}';
  }
}

class ProductModel {
  int? id = 0;
  String? imageUrl = "";
  int? cost = 0;
  String? description = "";
  String? date = "";

  ProductModel({
    this.id,
    this.imageUrl,
    this.cost,
    this.description,
    this.date,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      id: json["id"] ?? 0,
      imageUrl: json["imageUrl"] ?? "",
      cost: json["cost"] ?? 0,
      description: json["description"] ?? "",
      date: json["date"] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['imageUrl'] = imageUrl;
    data['cost'] = cost;
    data['description'] = description;
    data['date'] = date;
    return data;
  }

  @override
  String toString() {
    return 'ProductModel{id: $id, imageUrl: $imageUrl, cost: $cost, description: $description, date: $date}';
  }
}
